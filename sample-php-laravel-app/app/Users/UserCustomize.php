<?php

namespace App\Users;

use App\User;
use \IanOlson\Support\Traits\UploadTrait;

class UserCustomize extends User
{
	use UploadTrait;

    protected static $images = [
        'profile_pic',
    ];
	
    public function uploadImages($request)
    {
        $fill = [];
        foreach (self::$images as $image) {
            if ($request->hasFile($image)) {

                $file = $request->file($image);

                // TODO crop and resize

                $fill[$image] = $this->upload($file, ['disk' => 's3', 'directory' => $image]);
            }
        }
        return array_merge($request->all(), $fill);
    }
}
