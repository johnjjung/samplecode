<?php

namespace App;

use Storage;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \IanOlson\Support\Traits\UploadTrait;

class File extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'file_url',
    	'fileable_id',
    	'fileable_type',
    	'meta',
    	'fileable_id',
    	'portal_id',
    	'user_id'
    ];

    /**
     * Date/time fields
     * @var array
     */
    protected $dates = [
    	'deleted_at',
    	'created_at',
    	'updated_at'
    ];

    public static function boot()
    {
        File::creating(function ($file) {
            $file->name = $file->file_url->getClientOriginalName();
            $file->user_id = Auth::user()->id;
            $file->portal_id = Auth::user()->portal_id;
            $file_url = 'files/'.str_random(32);
            Storage::put(
                $file_url,
                file_get_contents($file->file_url->getRealPath())
            );
            $file->file_url = 'http://dashspace-dev.s3.amazonaws.com/'.$file_url;
        });
    }

    
    /**
     * format date with timezone
     * 
     * @return string
     */
    public function getUpdatedAttribute()
    {
        return $this->updated_at->timezone(Auth::user()->portal->timezone);
    }
    /**
     * format date with timezone
     * 
     * @return string
     */
    public function getCreatedAttribute()
    {
        return $this->created_at->timezone(Auth::user()->portal->timezone);
    }

    /**
     * This file belongs to a user
     * 
     * @return Relation
     */

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function portal()
    {
        return $this->belongsTo('App\Portal');
    }

    
    /**
     * A image belongs to a fileable object
     *
     * @return Relation
     */
    public function fileable()
    {
    	return $this->morphTo();
    }

}
