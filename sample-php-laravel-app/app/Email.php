<?php

namespace App;

use Mail;
use App\Portal;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Email extends Mail
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'subject',
        'html',
        'is_active',
        'recurring_type',
        'interval',
        'every',
        'on',
        'start_at',
        'end_at',
        'created_at',
        'portal_id',
        'user_id',
        'send_to_user_id',
        'send_to_account_id'
    ];

    /**
     * Date/time fields
     * @var array
     */
    protected $dates = [
        'start_at',
        'end_at',
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    public $type_array = [
        'onetime' => 'One-Time Scheduled',
        'recurring' => 'Recurring'
    ];

    public $interval_array = [
        'daily' => 'Daily',
        'weekly' => 'Weekly',
        'monthly' => 'Monthly',
        'yearly' => 'Yearly',
    ];

    public $on_week = [
        'monday' => 'Monday',
        'tuesday' => 'Tuesday',
        'wednesday' => 'Wednesday',
        'thursday' => 'Thursday',
        'friday' => 'Friday',
        'saturday' => 'Saturday',
        'sunday' => 'Sunday',
    ];

    public $on_month = [
       'january' => 'January',
       'february' => 'February',
       'march' => 'March',
       'april' => 'April',
       'may' => 'May',
       'june' => 'June',
       'july' => 'July',
       'august' => 'August',
       'september' => 'September',
       'october' => 'October',
       'november' => 'November',
       'december' => 'December',
    ];

    public $on_day = [
        '1' => '1st day of the month',
        '2' => '2nd day of the month',
        '3' => '3rd day of the month',
        '4' => '4th day of the month',
        '5' => '5th day of the month',
        '6' => '6th day of the month',
        '7' => '7th day of the month',
        '8' => '8th day of the month',
        '9' => '9th day of the month',
        '10' => '10th day of the month',
        '11' => '11th day of the month',
        '12' => '12th day of the month',
        '13' => '13th day of the month',
        '14' => '14th day of the month',
        '15' => '15th day of the month',
        '16' => '16th day of the month',
        '17' => '17th day of the month',
        '18' => '18th day of the month',
        '19' => '19th day of the month',
        '20' => '20th day of the month',
        '21' => '21th day of the month',
        '22' => '22th day of the month',
        '23' => '23th day of the month',
        '24' => '24th day of the month',
        '25' => '25th day of the month',
        '26' => '26th day of the month',
        '27' => '27th day of the month',
        '28' => '28th day of the month',
        'last' => 'Last day of the month',
    ];

    /**
     * Relationships
     * 
     * @return Relation
     */

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function portal()
    {
        return $this->belongsTo('App\Portal');
    }

    public function account()
    {
        return $this->belongsTo('App\Account');
    }

    public function setEndAtAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['end_at'] = null;
        }
    }

    /**
     * _send constructor
     *
     * try catch the email so it doesn't break on user side
     * 
     * @param  [type] $views  [description]
     * @param  [type] $swift  [description]
     * @param  [type] $events [description]
     * @return [type]         [description]
     */
    public static function _send($views, $swift, $events = null)
    {
        try
        {
            Mail::send($views, $swift, $events);
        }
        catch(Exception $e)
        {
            \Log::error('Email Error: ' . $e);
        }
    }

    

/**
 * Users
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

    /**
     * when a new user signs up for dashspace, confirm their email
     *
     * @param  object $user
     * @return void
     */
    public static function sendNewUserConfirmation($user)
    {
        \Log::info('Email.sendNewUserConfirmation');

    	// email new user sign up
    	self::_send('emails.user_signup', compact('user'), function($message) use ($user)
    	{
    	    $message->to($user->email, $name = $user->name);
    	    $message->subject('Welcome to Dashspace');
    	});
    }

    /**
     * when a new user is invited to dashspace, confirm their email
     *
     * @param  object $user
     * @return void
     */
    public static function sendInviteUser($user)
    {
        \Log::info('Email.sendInviteUser');

        // email new user sign up
        self::_send('emails.user_invite', compact('user'), function($message) use ($user)
        {
            $message->to($user->email, $name = $user->name);
            $message->subject('You\'ve been Invited to Join ' . $user->account->name);
        });
    }

/**
 * Reservations
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

    /**
     * when a user creates a reservation
     * 
     * @param  object $reservation
     * @param  object $user
     * @return void
     */
    public static function sendCreateReservationConfirmation($reservation, $user)
    {
        \Log::info('Email.sendCreateReservationConfirmation');

        // email new user sign up
        self::_send('emails.create_reservation', compact('user', 'reservation'), function($message) use ($user)
        {
            $message->to($user->email, $name = $user->name);
            $message->subject('Reservation Confirmation');
        });
    }

    /**
     * when a user edits a reservation
     * 
     * @param  object $reservation
     * @param  object $user
     * @return void
     */
    public static function sendEditReservationConfirmation($reservation, $user)
    {
        \Log::info('Email.sendEditReservationConfirmation');

        // email new user sign up
        self::_send('emails.edit_reservation', compact('user', 'reservation'), function($message) use ($user)
        {
            $message->to($user->email, $name = $user->name);
            $message->subject('Reservation Update Confirmation');
        });
    }

    /**
     * when a user deletes a reservation
     * 
     * @param  object $reservation
     * @param  object $user
     * @return void
     */
    public static function sendDeleteReservationConfirmation($reservation, $user)
    {
        \Log::info('Email.sendDeleteReservationConfirmation');

        // email new user sign up
        self::_send('emails.delete_reservation', compact('user', 'reservation'), function($message) use ($user)
        {
            $message->to($user->email, $name = $user->name);
            $message->subject('Reservation Delete Confirmation');
        });
    }


/**
 * Invoice
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

    /**
     * send user invoice available
     *
     * @param  object $user
     * @param  object $invoice - provider invoice object
     * @param  object $account 
     * @return void
     */
    public static function sendInvoice($invoice, $account, $user)
    {
        \Log::info('Email.sendInvoice');

        // find or create key
        $key = \App\Key::findOrCreate('Id', $invoice['Id'], 'Invoice', 'quickbooks', $account->id, $account->portal_id);

        // email new user sign up
        self::_send('emails.invoice', compact('user', 'account', 'invoice', 'key'), function($message) use ($user)
        {

            $message->to($user->email, $name = $user->name);
            $message->subject($user->portal->name.' Invoice Available');
        });

        // add to meta data that email was sent
        $key->addMeta(['Email' => ['sent_at' => Carbon::now()->toDateTimeString(), 'sent_by' => $user->portal->owner->id , 'sent_to' => $user->id ]]);
    }

    /**
     * send user invoice available
     *
     * @param  object $user
     * @param  object $invoice - provider invoice object
     * @param  object $account 
     * @return void
     */
    public static function sendReminderInvoice($invoice, $account, $user)
    {
        \Log::info('Email.sendReminderInvoice');

        // find or create key
        $key = \App\Key::findOrCreate('Id', $invoice['Id'], 'Invoice', 'quickbooks', $account->id);

        // email new user sign up
        self::_send('emails.invoice_due', compact('user', 'account', 'invoice', 'key'), function($message) use ($user)
        {
            $message->to($user->email, $name = $user->name);
            $message->subject(\Auth::user()->portal->name.' Invoice Available');
        });

        // add to meta data that email was sent
        $key->addMeta(['Email' => ['sent_at' => Carbon::now()->toDateTimeString(), 'sent_by' => \Auth::user()->id , 'sent_to' => $user->id ]]);
    }

    /**
     * send user invoice available
     *
     * @param  object $user
     * @param  object $invoice - provider invoice object
     * @param  object $account 
     * @return void
     */
    public static function sendOverdueInvoice($invoice, $account, $user)
    {
        \Log::info('Email.sendOverdueInvoice');

        // find or create key
        $key = \App\Key::findOrCreate('Id', $invoice['Id'], 'Invoice', 'quickbooks', $account->id);

        // email new user sign up
        self::_send('emails.invoice_overdue', compact('user', 'account', 'invoice', 'key'), function($message) use ($user)
        {
            $message->to($user->email, $name = $user->name);
            $message->subject(\Auth::user()->portal->name.' Invoice Available');
        });

        // add to meta data that email was sent
        $key->addMeta(['Email' => ['sent_at' => Carbon::now()->toDateTimeString(), 'sent_by' => \Auth::user()->id , 'sent_to' => $user->id ]]);
    }



/**
 * Payment
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

    // TODO credit card is expired

    /**
     * when a user makes a payment
     *
     * @param  object $user
     * @return void
     */
    public static function sendPayment($payment, $user)
    {
        \Log::info('Email.sendPayment');

        // email new user sign up
        self::_send('emails.make_payment', compact('user', 'payment'), function($message) use ($user)
        {
            $message->to($user->email, $name = $user->name);
            $message->subject($user->portal->name.' - Your Payment was made');
        });

    }

    public static function declinedPayment($payment, $user)
    {
        \Log::info('Email.declinedPayment');

        // email new user sign up
        self::_send('emails.declined_payment', compact('user', 'payment'), function($message) use ($user)
        {
            $message->to($user->email, $name = $user->name);
            $message->subject($user->portal->name.' - Your Payment was Declined');
        });

    }

    public static function receiveDeclinedPayment($payment, $user)
    {
        \Log::info('Email.declinedPayment');

        // email new user sign up
        self::_send('emails.admin_declined_payment', compact('user', 'payment'), function($message) use ($user)
        {
            $message->to(@$user->email, $name = @$user->name);
            $message->subject(@$user->portal->name.' - Payment Attempt was Declined');
        });

    }

    public static function sendAutoPayPayment($payment, $user)
    {
        \Log::info('Email.sendAutoPayPayment');

        // email new user sign up
        self::_send('emails.make_autopay_payment', compact('user', 'payment'), function($message) use ($user)
        {

            $message->to($user->email, $name = $user->name);
            $message->subject($user->portal->name.' - AutoPay Payment was made');
        });

    }

    public static function receivedPayment($payment, $user)
    {
        \Log::info('Email.receivedPayment');

        // email new user sign up
        self::_send('emails.received_payment', compact('user', 'payment'), function($message) use ($user)
        {

            $message->to($user->email, $name = $user->name);
            $message->subject('Received Payment from ' . $user->account->name);
        });

    }

    /**
     * when a payment gets a refund
     *
     * @param  object $user
     * @return void
     */
    public static function refundPayment($payment, $user)
    {
        \Log::info('Email.refundPayment');

        // email new user sign up
        self::_send('emails.cancel_payment', compact('user', 'payment'), function($message) use ($user)
        {
            $message->to($user->email, $name = $user->name);
            $message->subject($user->portal->name.' - Refund');
        });

    }

    /**
     * when a payment gets a refund
     *
     * @param  object $user
     * @return void
     */
    public static function sendRefundPayment($payment, $user)
    {
        \Log::info('Email.refundPayment');

        // email new user sign up
        self::_send('emails.admin_cancel_payment', compact('user', 'payment'), function($message) use ($user, $payment)
        {
            $message->to($user->email, $name = $user->name);
            $message->subject($user->portal->name.' - Refund To ' . $payment->account->name);
        });

    }

/**
 * Message
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

    /**
     * when a new user signs up for dashspace, confirm their email
     *
     * @param  object $user
     * @return void
     */
    public static function sendMessage($thread, $private_message, $user, $key)
    {
        \Log::info('Email.sendMessage');

        // email new user sign up
        self::_send('emails.message', compact('thread', 'private_message', 'user', 'key'), function($message) use ($user, $private_message)
        {
            $message->to($user->email, $name = $user->name);
            $message->subject('New Message from ' . $private_message->user->name);
        });

    }


/**
 * Post
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

    /**
     * replies to comment
     *
     * @param  object $user
     * @return void
     */
    public static function sendComment($post, $comment, $key)
    {
        \Log::info('Email.sendComment');

        // email new user sign up
        self::_send('emails.comment', compact('post', 'comment', 'key'), function($message) use ($post, $comment)
        {
            $message->to($post->user->email, $name = $post->user->name);
            $message->subject($comment->user->name . ' commented on your Post');
        });

    }


/**
 * Reminder
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

    /**
     * when a new user signs up for dashspace, confirm their email
     *
     * @param  object $user
     * @return void
     */
    public static function sendReminder($email)
    {
        \Log::info('Email.sendReminder');

        // email new user sign up
        self::_send('emails.reminder', compact('email'), function($message) use ($email)
        {
            $user = User::findOrFail($email->send_to_user_id);
            $message->to($user->email, $name = $user->name);
            $message->subject($email->subject);
        });

        // if onetime, deactivate
        if ($email->recurring_type == 'onetime')
        {
            $email->is_active = false;
        }
        // update occurences
        $email->occurences = $email->occurences + 1;
        $email->update();
    }
}
