<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incrementer extends Model
{
    protected $incrementer = null;
    private $once = true;

    /**
     * [getIncrementSetter description]
     * @param  $string {1}
     * @return $string 1
     */
    public function getIncrementSetter($string)
    {
    	$string = str_replace('{', '',$string);
    	$string = str_replace('}', '',$string[0]);
    	return $string[0];
    }

    public function set($set_var)
    {
    	// only set match once
    	if ($this->once)
    	{
    		if (ctype_digit($set_var))
    		{
	    		$this->incrementer = $set_var;
    		}
    		elseif (ctype_alpha($set_var))
    		{
    			$this->incrementer = $set_var;
    		}
    		else{
    			\Log::error($set_var." is not an integer or alpha");
    			$this->incrementer = $set_var;
    		}
    		$this->once = false;
    		return $this->incrementer;
    	}
    	return $this->incrementer;
    }

    public function get_tick()
    {
    	return $this->incrementer;
    }

    public function tickup()
    {
    	$this->incrementer++;
    	return $this->incrementer;
    }
    public function reset()
    {
    	$this->incrementer = null;
    	$this->once = true;
    }
}
