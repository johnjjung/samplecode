<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/* Tenant Routes */

// PreAuth
Route::get('/', function () { return Redirect::to('/auth/login' ); });
Route::get('/login/{portal_namespace?}', 'Auth\AuthController@login');
Route::controllers(['auth' => 'Auth\AuthController', 'password' => 'Auth\PasswordController', ]);

// Users
Route::get('users/verify/{token}', 'UsersController@verify');
Route::get('users/invite/{token}', 'UsersController@invite');
Route::post('users/invited/{token}', 'UsersController@invited');
Route::post('users/{id}/reinvite', 'UsersController@reinvite');
Route::get('users/profile', function() { return redirect(route('users.edit', Auth::user()->id)); });
Route::get('users/dashboard', 'UsersController@dashboard');
Route::put('users/toggle/{id}', 'UsersController@toggle');
Route::get('users/myprofile', function() {
	return redirect('users/' . Auth::user()->id . '/edit');
});

// Accounts
Route::get('accounts/profile', function() { return redirect(route('accounts.edit', Auth::user()->account_id)); });
Route::get('accounts/{id}/users', 'AccountsController@users');
Route::get('accounts/{id}/files', 'AccountsController@files');

// Messages
Route::group(['prefix' => 'messages'], function () {
    Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
    Route::get('/read_all', ['as' => 'messages.read_all', 'uses' => 'MessagesController@read_all']);
    Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
    Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
    Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
    Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
});

// Reservations
Route::get('reservations/calendars', 'ReservationsController@calendars');
Route::get('reservations/events', 'ReservationsController@events');

// Calendars
Route::get('calendars/reserve', 'CalendarsController@reserve');

// Comments
Route::get('comments/{id}/post', 'CommentsController@post');

// Likes
Route::post('likes/{id}/{type}', 'LikesController@unlike');

// Invoices
Route::get('invoices/download/{invoice_id}', 'InvoicesController@download');
Route::get('invoices/see/{invoice_id}', 'InvoicesController@see');

// PaymentAccounts
Route::get('payment_accounts/method/{type}', 'PaymentAccountsController@method');
Route::put('payment_accounts/primary/{id}', 'PaymentAccountsController@primary');
Route::put('payment_accounts/autopay', 'PaymentAccountsController@update_autopay');
Route::get('payment_accounts/autopay', 'PaymentAccountsController@autopay');

// RecurringPayments
Route::put('recurring_payments/subtotal', 'RecurringPaymentsController@subtotal');

// Message
Route::get('keys/email/{token}', 'KeysController@email');

// Resourceful
Route::resource('reservations', 'ReservationsController');
Route::resource('messages', 'MessagesController');
Route::resource('spaces', 'SpacesController');
Route::resource('accounts', 'AccountsController');
Route::resource('users', 'UsersController');
Route::resource('tags', 'TagsController');
Route::resource('posts', 'PostsController');
Route::resource('pages', 'PagesController');
Route::resource('comments', 'CommentsController');
Route::resource('files', 'FilesController');
Route::resource('likes', 'LikesController');
Route::resource('invoices', 'InvoicesController');
Route::resource('payment_accounts', 'PaymentAccountsController');
Route::resource('payments', 'PaymentsController');
Route::resource('recurring_payments', 'RecurringPaymentsController');

// Entrust User
// Entrust::routeNeedsRole('invoices*', ['member_owner', 'member_admin'], Redirect::to('/'), false);
Entrust::routeNeedsRole('payment_accounts*', ['member_owner', 'member_admin'], Redirect::to('/'), false);
Entrust::routeNeedsRole('payments*', ['member_owner', 'member_admin'], Redirect::to('/'), false);
Entrust::routeNeedsRole('recurring_payments*', ['member_owner', 'member_admin'], Redirect::to('/'), false);


/* Admin Routes */
Route::group(array('prefix' => 'admin', 'namespace' => 'Admin'), function()
{

	// Users
	Route::patch('users/assign', 'AdminUsersController@assign');
	Route::put('users/role_update/{id}', 'AdminUsersController@role_update');
	Route::get('users/myprofile', function() {
		return redirect('admin/users/' . Auth::user()->id . '/edit');
	});
	Route::get('users/{id}/notes', 'AdminUsersController@notes');
	Route::post('users/{id}/reinvite', 'AdminUsersController@reinvite');
	// Accounts
	Route::get('accounts/my_account', 'AdminAccountsController@edit');
	Route::get('accounts/spaces', 'AdminAccountsController@spaces');
	Route::get('accounts/invoices', 'AdminAccountsController@invoices');
	Route::get('accounts/payments', 'AdminAccountsController@payments');
	Route::patch('accounts/make_primary', 'AdminAccountsController@makePrimary');
	Route::get('accounts/{id}/users', 'AdminAccountsController@users');
	Route::get('accounts/{id}/accounting', 'AdminAccountsController@accounting');
	Route::get('accounts/{id}/notes', 'AdminAccountsController@notes');
	Route::get('accounts/{id}/files', 'AdminAccountsController@files');
	Route::put('accounts/toggle/{id}', 'AdminAccountsController@toggle');
	// Spaces
	Route::get('spaces/create_many', 'AdminSpacesController@createMany');
	Route::get('spaces/{id}/notes', 'AdminSpacesController@notes');
	Route::put('spaces/assign/{id}', 'AdminSpacesController@assign');
	// Reservations
	Route::get('reservations/calendars', 'AdminReservationsController@calendars');
	// Calendars
	Route::get('calendars/reserve', 'AdminCalendarsController@reserve');
	// Portals
	Route::get('portals/preauth_preview', 'AdminPortalsController@preauth_preview');
	Route::get('portals/manage', 'AdminPortalsController@edit');
	Route::get('portals/payments', 'AdminPortalsController@payments');
	Route::post('portals/portal_update', 'AdminPortalsController@portal_update');
	Route::get('portals/calendars', 'AdminPortalsController@calendars');
	Route::get('portals/accounting', 'AdminPortalsController@accounting');
	Route::get('portals/customize', 'AdminPortalsController@customize');
	// Dashboard
	Route::get('dashboards/preview', 'AdminDashboardsController@preview');
	Route::get('dashboards/reset', 'AdminDashboardsController@reset');
	// Quickbooks
	// App OAuth
	Route::get('quickbooks/appauth', 'AdminQuickbooksController@appauth');	
	Route::get('quickbooks/appauthed', 'AdminQuickbooksController@appauthed');	
	Route::get('quickbooks/qboauth', 'AdminQuickbooksController@qboauth');	
	Route::get('quickbooks/qboauthed', 'AdminQuickbooksController@qboauthed');	
	Route::get('quickbooks/disconnect', 'AdminQuickbooksController@disconnect');
	// Customers
	Route::get('quickbooks/customers', 'AdminQuickbooksController@customers');
	Route::post('quickbooks/customer_create/{quickbooks_customer_id}','AdminQuickbooksController@customer_create');
	Route::post('quickbooks/customer_delete/{quickbooks_customer_id}','AdminQuickbooksController@customer_delete');
	Route::post('quickbooks/customer_update/{quickbooks_customer_id}','AdminQuickbooksController@customer_update');
	Route::post('quickbooks/customer_sync/{quickbooks_customer_id}','AdminQuickbooksController@customer_sync');
	Route::post('quickbooks/create_quickbooks_customer/{account_id}', 'AdminQuickbooksController@create_quickbooks_customer');
	Route::post('quickbooks/update_quickbooks_customer/{account_id}', 'AdminQuickbooksController@update_quickbooks_customer');
	// Invoices
	Route::get('invoices/download/{invoice_id}', 'AdminInvoicesController@download');
	Route::post('invoices/email/{invoice_id}', 'AdminInvoicesController@email');
	// Recurring Invoices
	Route::post('recurring_invoices/toggle/{id}', 'AdminRecurringInvoicesController@toggle');
	Route::get('recurring_invoices/interval', 'AdminRecurringInvoicesController@interval');
	// Payments
	Route::post('payments/check/{transaction_id}', 'AdminPaymentsController@check');
	Route::post('payments/void/{transaction_id}', 'AdminPaymentsController@void');
	// PaymentAccounts
	Route::get('payment_accounts/method/{type}', 'AdminPaymentAccountsController@method');
	Route::put('payment_accounts/primary/{type}', 'AdminPaymentAccountsController@primary');
	// Resourceful
	Route::resource('calendars', 'AdminCalendarsController');
	Route::resource('reservations', 'AdminReservationsController');
	Route::resource('spaces', 'AdminSpacesController');
	Route::resource('portals', 'AdminPortalsController');
	Route::resource('accounts', 'AdminAccountsController');
	Route::resource('users', 'AdminUsersController');
	Route::resource('tags', 'AdminTagsController');
	Route::resource('pages', 'AdminPagesController');
	Route::resource('notes', 'AdminNotesController');
	Route::resource('messages', 'AdminMessagesController');
	Route::resource('dashboards', 'AdminDashboardsController');
	Route::resource('keys', 'AdminKeysController');
	Route::resource('payments', 'AdminPaymentsController');
	Route::resource('payment_accounts', 'AdminPaymentAccountsController');
	Route::resource('invoices', 'AdminInvoicesController');
	Route::resource('files', 'AdminFilesController');
	Route::resource('emails', 'AdminEmailsController');
	Route::resource('recurring_invoices', 'AdminRecurringInvoicesController');
	Route::resource('recurring_payments', 'AdminRecurringPaymentsController');
});

// Entrust Admin
Entrust::routeNeedsRole('portals/edit', ['portal_admin','portal_owner','root'], Redirect::to('/'), false);
Entrust::routeNeedsRole('admin/spaces*', ['portal_employee','portal_admin','portal_owner','root'], Redirect::to('/'), false);
Entrust::routeNeedsRole('admin/calendars*', ['portal_employee','portal_admin','portal_owner','root'], Redirect::to('/'), false);
Entrust::routeNeedsRole('admin/reservations*', ['portal_employee','portal_admin','portal_owner','root'], Redirect::to('/'), false);
Entrust::routeNeedsRole('admin/accounts*', ['portal_employee','portal_admin','portal_owner','root'], Redirect::to('/'), false);
Entrust::routeNeedsRole('admin/users*', ['portal_employee','portal_admin','portal_owner','root'], Redirect::to('/'), false);
Entrust::routeNeedsRole('admin/tags*', ['portal_employee','portal_admin','portal_owner','root'], Redirect::to('/'), false);
Entrust::routeNeedsRole('admin/messages*', ['portal_employee','portal_admin','portal_owner','root'], Redirect::to('/'), false);
Entrust::routeNeedsRole('admin/pages*', ['portal_employee','portal_admin','portal_owner','root'], Redirect::to('/'), false);
Entrust::routeNeedsRole('admin/invoices*', ['portal_admin','portal_owner','root'], Redirect::to('/'), false);
Entrust::routeNeedsRole('admin/quickbooks*', ['portal_admin','portal_owner','root'], Redirect::to('/'), false);
Entrust::routeNeedsRole('admin/dashboards*', ['portal_admin','portal_owner','root'], Redirect::to('/'), false);
Entrust::routeNeedsRole('admin/keys*', ['portal_admin','portal_owner','root'], Redirect::to('/'), false);
Entrust::routeNeedsRole('admin/payments*', ['portal_admin','portal_owner','root'], Redirect::to('/'), false);
Entrust::routeNeedsRole('admin/payment_accounts*', ['portal_admin','portal_owner','root'], Redirect::to('/'), false);
Entrust::routeNeedsRole('admin/files*', ['portal_admin','portal_owner','root'], Redirect::to('/'), false);
Entrust::routeNeedsRole('admin/emails*', ['portal_admin','portal_owner','root'], Redirect::to('/'), false);
Entrust::routeNeedsRole('admin/recurring_invoices*', ['portal_admin','portal_owner','root'], Redirect::to('/'), false);
Entrust::routeNeedsRole('admin/recurring_payments*', ['portal_admin','portal_owner','root'], Redirect::to('/'), false);

