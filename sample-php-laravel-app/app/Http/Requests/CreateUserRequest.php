<?php

namespace App\Http\Requests;

use App\User;
use App\Http\Requests\Request;

class CreateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Entrust::hasRole(['portal_owner', 'portal_admin', 'portal_employee', 'member_owner', 'member_admin']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::find($this->users);

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            case 'PATCH':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'name' => 'required|max:255',
                    'email' => 'required|email|unique:users,email',
                    'account_id' => 'required',
                    'portal_id' => 'required'
                ];
            }
            case 'PUT':
            {
                return [
                    'name' => 'required|max:255',
                    'email' => 'required|email|unique:users,email,'.$user->id,
                    'portal_id' => 'required',
                ];
            }
            default:break;
        }


    }
}
