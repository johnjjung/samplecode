<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Note;
use App\Post;
use App\Role;
use App\Email;
use App\Account;
use App\Dashboard;
use App\Users\UserCustomize;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenicateUsers;
use App\Events\UserWasInvited as UserWasInvited;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest as CreateUserRequest;
use App\Http\Requests\InviteRequest as InviteRequest;

class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => 'invite, invited, verify']);
    }

    /**
     * Display a user's dashboard
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard(Dashboard $dashboard)
    {
        $role = @Auth::user()->roles->first()->name;
        $dashboards = $dashboard
            ->where('portal_id', Auth::user()->portal_id)
            ->where('roles', 'LIKE', "%".@$role.",%")
            ->get();
        return view('users.dashboard', compact('dashboards'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        \Log::info('users.edit');
        $user = User::findOrFail($id);
        $accounts = Account::all()->where('portal_id', $user->portal_id)->lists('name');
        $notes = Note::where('portal_id', $user->portal_id)
            ->where('notable_type', 'App\User')
            ->where('notable_id', $user->id)
            ->whereNull('deleted_at')
            ->orderBy('created_at', 'desc')
            ->paginate(20);
            $roles = [
                'member_admin' => 'Member Administrator',
                'member' =>   'Member'
            ];

        return view('users.edit', compact('user', 'accounts', 'notes', 'roles'));
    }



    /**
     * Confirms the User's email and updates the is_confirmed to true
     *
     * @return redirected to the dashboard
     */
    public function verify($token)
    {        
        $user = User::where('token', $token)->first();
        
        $user->is_confirmed = true;
        $user->is_active = true;
        if ($user->save())
        {   
            if (Auth::check())
            {
                flash()->success('Email has been confirmed!');
                return redirect('users/dashboard');
            }
            else
            {
                // log them in
                Auth::loginUsingId($user->id);
                flash()->success('Email has been confirmed!');
                return redirect('users/dashboard');
            }
        }

    }

    /**
     * Confirms the User's email and updates the is_confirmed to true
     *
     * @return redirected to the dashboard
     */
    public function invite($token)
    {        
        $user = User::where('token', $token)->first();
        
        $user->is_confirmed = true;
        if ($user->save())
        {
            return view('users.invited', compact('token'));
        }

    }

    /**
     * Sets the user's password and logs them in
     *
     * @return redirected to the dashboard
     */
    public function invited(InviteRequest $request, $token)
    {        
        $user = User::where('token', $token)->first();

        if($user->fill(['password' => bcrypt($request->password)])->save())
        {
            // log them in
            Auth::loginUsingId($user->id);
            flash()->success('Email has been confirmed!');
            return redirect('users/dashboard');
        }

    }

    public function toggle(Request $request, $id)
    {
        $user = User::find($id);
        if ($user->id != Auth::user()->id)
        {
            if ($user->fill(['is_active' => $request->is_active])->update())
            {
                flash()->success('User Status Updated');
            }
        }
        else
        {
            flash()->success('You can\'t Deactivate your own Account');
        }
        return back();

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('Account')
            ->where('portal_id', Auth::user()->portal_id)
            ->orderBy('updated_at', 'desc')
            ->paginate(20);
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = [
            'member_admin' =>     'Member Administrator',
            'member' =>   'Member'
        ];
        return view('users.create', compact('roles'));
    }

    public function reinivite($id)
    {
        \Log::info('UsersController.reinvite');
        $user = User::find($id);
        if ($user)
        {
            // invite
            Email::sendInviteUser($user);
        }
        flash()->success('User has been Invited!');
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        \Log::info('users.store()');
        $inputs = $request->all();
        
        $inputs['password'] = bcrypt(str_random(32));
        $inputs['token'] = str_random(60);
        $user = User::create($inputs);
        // set user role
        if ($inputs['role'])
        {
            $role_set = $user->assignRole($inputs['role']);
        }

        if ($user)
        {
        	if ($request->send_invite)
        	{
        		flash()->success('User has been Invited!');
        	    event(new UserWasInvited($user));
        	}
        	else
        	{
        		flash()->success('User has been Created!');
        	}

            return redirect('accounts/' . $user->account_id . '/users');
        }
    }

    public function reinvite($id)
    {
        $user = User::findOrFail($id);
        event(new UserWasInvited($user));
        flash()->success('Invitation Sent!');
        return back();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $posts = Post::where('user_id', $id)
            ->orderBy('updated_at', 'desc')
            ->paginate();
        if (\Request::ajax())
        {
            return view('posts.activity_list', compact('user', 'posts'));    
        }
        return view('users.show', compact('user', 'posts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $user = User::findOrFail(Auth::user()->id);
        $accounts = Account::all()->where('portal_id', $user->portal_id)->lists('name');
            
        return view('users.edit', compact('user', 'accounts', 'notes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function assign(CreateUserRequest $request)
    {
        \Log::info('users.assign()');
        $user = User::where('email', $request->user_id)->first();

        if ($user->update(['account_id' => $request->account_id]))
        {
            flash()->success('User has been Updated!');
            return back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateUserRequest $request, UserCustomize $customize, $id)
    {
        \Log::info('accounts.update()');
        $user = User::findOrFail($id);
        // set user role
        if ($request->role)
        {
            $user->assignRole($request->role);
        }
        // upload files after validation
        $data = $customize->uploadImages($request);

        if ($user->update($data))
        {
            flash()->success('User has been Updated!');
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if ($user->id != Auth::user()->id)
        {
            $user->delete();
            flash()->success('User has been deleted!');
            return redirect('users');
        }
        else
        {
            flash()->success('You cannot delete your own account.');
            return back();
        }
    }
}
