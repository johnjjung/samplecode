<?php

namespace App;

use DB;
use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cmgmyr\Messenger\Traits\Messagable;


use Auth;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, EntrustUserTrait, SoftDeletes, Messagable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'description',
        'title',
        'password',
        'token',
        'profile_pic',
        'is_confirmed',
        'is_active',
        'account_id',
        'portal_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     * @var array
     */
    protected $hidden = [
        'password',
        'tokens',
        'remember_token'
    ];

    
    /**
     * format date with timezone
     * 
     * @return string
     */
    public function getUpdatedAttribute()
    {
        return $this->updated_at->timezone(Auth::user()->portal->timezone);
    }
    /**
     * format date with timezone
     * 
     * @return string
     */
    public function getCreatedAttribute()
    {
        return $this->created_at->timezone(Auth::user()->portal->timezone);
    }

    public function getInitialsAttribute()
    {
        $words = explode(" ", $this->name);
        $acronym = "";

        foreach ($words as $w) {
          $acronym .= $w[0];
        }
        return strtoupper($acronym);
    }


    public function getProfilePicAttribute($value)
    {
        if (empty($value))
        {
            return 'http://dummyimage.com/300x300/f3f3f3/0084ff.png&text='.$this->initials;
        }
        else
        {
            return $value;
        }
    }

    public function getUnreadCountAttribute()
    {
        return $this->messages()->whereNull('read_at')->count();
    }

    public function accountOwner()
    {
        return $this->hasOne('App\Account');
    }

    public function account()
    {
        return $this->belongsTo('App\Account');
    }

    public function portal()
    {
        return $this->belongsTo('App\Portal');
    }
    public function space()
    {
        return $this->belongsTo('App\Space');
    }

    public function reservations()
    {
        return $this->belongsTo('App\Reservation');
    }

    public function portalOwner()
    {
        return $this->hasOne('App\Portal');
    }

    public function notes()
    {
        return $this->hasOne('App\Note');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function messages()
    {
        return $this->morphMany('App\Message', 'messageable');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }


    /**
     * gets a list of user names with emails
     * @return array list (id => 'name (email)')
     */
    public static function getUserEmailList()
    {
        $users = User::where('portal_id', Auth::user()->portal_id)
            ->get();
        foreach ($users as $key => $user) {
            $users[$key] = $user->name . " (" . $user->email .")";
        }
        return $users;
    }

    /**
     * attaches and removes roles
     * @param  $role name
     * @return
     */
    public function assignRole($role)
    {
    	if (!empty($role))
    	{
    		// if we're assigning a new owner then make the current owner an admin
    		if ($role == 'member_owner' || $role == 'portal_owner')
    		{
                $current_owner = self::find($this->account->user_id);
                \Log::info($current_owner);
    			// update to reassign role
                if (!empty($current_owner))
                {
                    // detach all roles of previous owner
        			$current_owner->detachRoles(Role::all());

                    // reassign the previous owner
        			if ($role == 'member_owner')
        			{
    	    			$current_owner->attachRole(Role::where('name', 'member_admin')->first()->id);
        			}
        			if ($role == 'portal_owner')
        			{
    	    			$current_owner->attachRole(Role::where('name', 'portal_admin')->first()->id);
        			}
                }

                // update to assign this user as owner
                $this->account->user_id = $this->id;
                $this->account->update();
    		}
    		

    		// detaches all roles
    		$this->detachRoles(Role::all());
            // attach the new role to the current user
    		$this->attachRole(Role::where('name', $role)->first()->id);

    		return true;
    	}
    	return false;
    }
}
