<?php

namespace App\Console\Commands;

use App\Email;
use Illuminate\Console\Command;
use Carbon\Carbon;

class SendEmailReminders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:reminders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks Emails to Send out Today';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * put in a 5 minute padding incase cron takes long
     * @return mixed
     */
    public function handle()
    {

        // Assumes that the CRON job will run in UTC at every hour
        Carbon::setTestNow(Carbon::parse('2016-01-27 03:01:00'));

        // TODO
        // var_dump(Carbon::now());
        // var_dump(Carbon::now()->toDateString());
        // var_dump(Carbon::now()->hour);
        
        // // get emails that need reminding now
        // $emails = Email::getEmailsToSend();        
        
        // dd($emails->count());

        // // loop through and send out reminders
        // foreach ($emails as $email) {
        //     Email::sendReminder($email);
        // }

    }
}
