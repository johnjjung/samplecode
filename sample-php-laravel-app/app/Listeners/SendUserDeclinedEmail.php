<?php

namespace App\Listeners;

use App\Email;
use App\Events\PaymentWasDeclined;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendUserDeclinedEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaymentWasDeclined  $event
     * @return void
     */
    public function handle(PaymentWasDeclined $event)
    {
        Email::declinedPayment($event->payment, $event->payment->account->primary);
    }
}
