<?php

namespace App\Listeners;

use App\Events\PaymentWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Email;

class SendUserPaymentEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaymentWasCreated  $event
     * @return void
     */
    public function handle(PaymentWasCreated $event)
    {
        \Log::info('SendUserPaymentEmail');

        // Emails the user who made the payment
        // if (@$event->payment->user->id != @$event->payment->account->primary->id)
        // {
        //     \Log::info('email user who made payment');
        //     Email::sendPayment($event->payment, $event->payment->user);
        // }
        // Emails the primary account holder
        Email::sendPayment($event->payment, $event->payment->account->primary);
    }
}
