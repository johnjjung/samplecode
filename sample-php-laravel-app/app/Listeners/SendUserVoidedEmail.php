<?php

namespace App\Listeners;

use App\Events\PaymentWasVoided;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Email;

class SendUserVoidedEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaymentWasVoided  $event
     * @return void
     */
    public function handle(PaymentWasVoided $event)
    {
        \Log::info('SendUserPaymentEmail');

        \Log::info($event->payment);
        \Log::info($event->payment->user);
        \Log::info($event->payment->account);
        \Log::info($event->payment->account->primary);

        // Emails the user who made the payment
        if (@$event->payment->user->id != @$event->payment->account->primary->id)
        {
            Email::refundPayment(@$event->payment, @$event->payment->user);
        }
        // Emails the primary account holder
        Email::refundPayment(@$event->payment, @$event->payment->account->primary);
    }
}
