<?php

namespace App\Listeners;

use App\Email;
use App\Events\UserWasInvited;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendUserWelcomeEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserWasInvited  $event
     * @return void
     */
    public function handle(UserWasInvited $event)
    {
        \Log::info('SendUserWelcomeEmail');

        // auto activates the user
        if (!$event->user->is_active)
        {
            $event->user->is_active = true;
            $event->user->save();
        }

        // check if user token exists
        if (empty($event->user->token)) 
        {
            $event->user->token = str_random(60);
            $event->user->save();
        }
        Email::sendInviteUser($event->user);
    }
}
