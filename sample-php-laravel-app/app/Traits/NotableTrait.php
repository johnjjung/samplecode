<?php

trait NotableTrait {

	/**
	 * This has many tags
	 *
	 * @return Relation
	 */
	public function notes()
	{
		return $this->morphMany('App\Note', 'notable');
	}

	/**
	 * Return number of tags for this object
	 *
	 * @return int 
	 */
	public function notes_count()
	{
		return $this->getNoteCountAttribute();
	}

	public function getNoteCountAttribute()
	{
		return $this->notes()->count();
	}
}