<?php

trait CarbonizeTrait {

	/**
	 * format start_time to 02:30 am
	 * 
	 * @return string
	 */
	public function getCreatedCarbonAttribute()
	{
	    return Carbon::parse($this->created_at);
	}

	/**
	 * format start_time to 02:30 am
	 * 
	 * @return string
	 */
	public function getUpdatedCarbonAttribute()
	{
	    return Carbon::parse($this->updated_at);
	}
}