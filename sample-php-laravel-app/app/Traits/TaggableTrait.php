<?php

trait TaggableTrait {

	/**
	 * This has many tags
	 *
	 * @return Relation
	 */
	public function tags()
	{
		return $this->morphMany('App\Tag', 'taggable');
	}

	/**
	 * Return number of tags for this object
	 *
	 * @return int 
	 */
	public function tags_count()
	{
		return $this->getTagCountAttribute();
	}

	public function getTagCountAttribute()
	{
		return $this->tags()->count();
	}
}