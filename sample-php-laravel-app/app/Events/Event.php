<?php

namespace App\Events;

abstract class Event
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        // 'App\Events\PodcastWasPurchased' => [
        //     'App\Listeners\EmailPurchaseConfirmation',
        // ],
    ];
}
