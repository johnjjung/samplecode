<?php
namespace Codeception\Module;

// here you can define custom functions for CodeGuy 

class CodeHelper extends \Codeception\Module
{
	public function seeFullCurrentURLEquals($value)
	{
	    return ($this->getModule('PhpBrowser')->client->getHistory()->current()->getUri() == $value);
	}

	public function amLoggedAs($user, $url = '/')
	{
		$I = $this->getModule('WebDriver');
        $I->amOnPage($url);

        if (is_array($user))
        {
	        $I->fillField('#email', $user['email']);
	        $I->fillField('#password', $user['password']);
        }
        elseif (is_string($user))
        {
        	$I->fillField('#email', $user);
        	$I->fillField('#password', 'asdfasdf');
        }

        $I->click('Login');
    }

    public function logout()
    {
        $I = $this->getModule('WebDriver');
        $I->amOnPage('/logout');
    }

}
