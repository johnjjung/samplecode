<?php  //[STAMP] a58c641376d1cc22d6c01ba160bcc6b8
namespace _generated;

// This class was automatically generated by build task
// You should not change it manually as it will be overwritten on next build
// @codingStandardsIgnoreFile

use Codeception\Module\CodeHelper;

trait CodeGuyActions
{
    /**
     * @return \Codeception\Scenario
     */
    abstract protected function getScenario();

    
    /**
     * [!] Method is generated. Documentation taken from corresponding module.
     *
     *
     * Conditional Assertion: Test won't be stopped on fail
     * @see \Codeception\Module\CodeHelper::seeFullCurrentURLEquals()
     */
    public function canSeeFullCurrentURLEquals($value) {
        return $this->getScenario()->runStep(new \Codeception\Step\ConditionalAssertion('seeFullCurrentURLEquals', func_get_args()));
    }
    /**
     * [!] Method is generated. Documentation taken from corresponding module.
     *
     *
     * @see \Codeception\Module\CodeHelper::seeFullCurrentURLEquals()
     */
    public function seeFullCurrentURLEquals($value) {
        return $this->getScenario()->runStep(new \Codeception\Step\Assertion('seeFullCurrentURLEquals', func_get_args()));
    }

 
    /**
     * [!] Method is generated. Documentation taken from corresponding module.
     *
     *
     * @see \Codeception\Module\CodeHelper::amLoggedAs()
     */
    public function amLoggedAs($user, $url = null) {
        return $this->getScenario()->runStep(new \Codeception\Step\Condition('amLoggedAs', func_get_args()));
    }

 
    /**
     * [!] Method is generated. Documentation taken from corresponding module.
     *
     *
     * @see \Codeception\Module\CodeHelper::logout()
     */
    public function logout() {
        return $this->getScenario()->runStep(new \Codeception\Step\Action('logout', func_get_args()));
    }
}
