<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class SelectizeHelper extends \Codeception\Module
{

	/**
	 * Selects an option in a Chosen Selector based on its label
	 *
	 * @return void
	 */
	public function selectOptionInSelectize($label, $option)
	{
		$I = $this;
		$I->waitForJS("return jQuery(\"label:contains('$label')\");");
		$selectID = $I->executeJS("return jQuery(\"label:contains('$label')\").attr(\"for\");");
		$selectizeSelectID = $selectID . '_selectize';
		$dropwdownId = $selectID . '_selectize_options';
		$I->waitForElement(['id' => $selectizeSelectID], 60);
		$I->comment("I open the $label chosen selector");

		$I->click(['xpath' => "//div[@id='$selectizeSelectID']/div"]);
		$I->waitForElement(['xpath' => "//div[@id='$selectizeSelectID']//input"], 60);
		$I->pressKey(['xpath' => "//div[@id='$selectizeSelectID']//input"], \Facebook\WebDriver\WebDriverKeys::BACKSPACE);
		$I->wait(1);
		$I->fillField(['xpath' => "//div[@id='$selectizeSelectID']//input"], $option);
		$I->waitForElement(['xpath' => "//div[@id='$dropwdownId']/div[@class='selectize-dropdown-content']/div[@data-title='$option']"], 60);
		$I->click(['xpath' => "//div[@id='$dropwdownId']/div[@class='selectize-dropdown-content']/div[@data-title='$option']"]);

		// Gives time to selectize to close
		$I->wait(1);
	}

}
