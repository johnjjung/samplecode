<?php 

class UserSignUpCest
{

    public function _before(\WebGuy $I)
    {
	    $I->amOnPage('/');

    }

    public function _after(\WebGuy $I)
    {
    }

    /**
     * user sign up tests
     */
    public function userSignUp(\WebGuy $I) 
    {
		$I->see('Dashspace');
		$I->see('Create an account');
		$I->click('Create an account');
		$I->see('Open a DashSpace Account');
		$I->fillField(['name' => 'name'], 'Test Name');
		$I->fillField(['name' => 'email'], 'testuser@email.com');
		$I->fillField(['name' => 'password'], 'asdfasdf');
		$I->fillField(['name' => 'password_confirmation'], 'asdfasdf');
		$I->executeJS('$(".icheckbox_square-green").addClass("checked")');
		$I->executeJS('$("#terms").prop("checked", true)');

		$I->click('Register');
		$I->see('Dashboard');
    }

    /**
     * @depends userSignUp
     */
    public function userLogin(\WebGuy $I) 
    {
		$I->see('Dashspace');
		$I->see('Login');
		$I->fillField(['name' => 'email'], 'owner@email.com');
		$I->fillField(['name' => 'password'], 'asdfasdf');
		$I->click('Login');
		$I->see('Dashboard');
    }

    /**
     * user login test
     */
    public function userFailLogin(\WebGuy $I) 
    {
		$I->see('Dashspace');
		$I->see('Login');
		$I->fillField(['name' => 'email'], 'sadfasd@asdfasdf.com');
		$I->fillField(['name' => 'password'], 'fdasfasdfsadf');
		$I->click('Login');
		$I->see('These credentials do not match our records.');

    }

    /**
     * user login test
     */
    public function userFailLoginActivation(\WebGuy $I) 
    {
		$I->see('Dashspace');
		$I->see('Login');
		$I->fillField(['name' => 'email'], 'notactiveuser@email.com');
		$I->fillField(['name' => 'password'], 'asdfasdf');
		$I->click('Login');
		$I->see('User or Account is not Activated');

    }

    
}

	
