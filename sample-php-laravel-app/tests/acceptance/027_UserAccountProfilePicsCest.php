<?php


class UserAccountProfilePicsCest
{
    public function _before(WebGuy $I)
    {
    }

    public function _after(WebGuy $I)
    {
    }

    // upload user image admin
    public function userAdminImageUploadTest(WebGuy $I)
    {
        $I->amLoggedAs([
            'email' => 'owner@email.com',
            'password' => 'asdfasdf'
        ]);
        $I->click('//*[@id="side-menu"]/li[1]/div[2]/a/span/span[2]');
        $I->click('My Profile');
        $I->attachFile('//*[@id="profile_pic"]', 'profilepic.jpg');
        $I->click('Save changes');
        $I->waitForText('User has been Updated!');

    }

    // upload account image admin
    public function accountAdminImageUploadTest(WebGuy $I)
    {
        $I->amLoggedAs([
            'email' => 'owner@email.com',
            'password' => 'asdfasdf'
        ]);
        $I->click('//*[@id="side-menu"]/li[1]/div[2]/a/span/span[2]');
        $I->click('Manage Account');
        $I->attachFile('//*[@id="profile_pic"]', 'profilepic.jpg');
        $I->click('Save');
        $I->waitForText('Account has been Updated!');
    }

    // upload user image user
    public function userMemberImageUploadTest(WebGuy $I)
    {
        $I->amLoggedAs([
            'email' => 'member_owner@email.com',
            'password' => 'asdfasdf'
        ]);
        $I->click('//*[@id="side-menu"]/li[1]/div[2]/a/span/span[2]');
        $I->click('My Profile');
        $I->attachFile('//*[@id="profile_pic"]', 'profilepic.jpg');
        $I->click('Save changes');
        $I->waitForText('User has been Updated!');

    }
    // upload account image user
    public function accountMemberImageUploadTest(WebGuy $I)
    {
        $I->amLoggedAs([
            'email' => 'member_owner@email.com',
            'password' => 'asdfasdf'
        ]);
        $I->click('//*[@id="side-menu"]/li[1]/div[2]/a/span/span[2]');
        $I->click('Manage Account');
        $I->attachFile('//*[@id="profile_pic"]', 'profilepic.jpg');
        $I->click('Save');
        $I->waitForText('Account has been Updated!');
    }
}
