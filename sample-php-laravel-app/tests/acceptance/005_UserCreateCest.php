<?php
use \WebGuy;

class UserCreateCest
{

    public function _cleanup()
    {
        $this->getModule('Db')->_reconfigure(array('cleanup' => 'false'));
    }
    public function _before(WebGuy $I)
    {
        
    }

    public function _after(WebGuy $I)
    {
    }

    /**
     * 
     */
    public function cannotDeleteUser(WebGuy $I)
    {
        $I->amLoggedAs([
            'email' => 'member_admin@email.com',
            'password' => 'asdfasdf'
        ]);
        $I->amOnPage('/users/59/edit');
        $I->wait(2);
        $I->see('Edit User');
        $I->click('Delete');
        $I->see('You cannot delete your own account.');

    }

    /**
     * 
     */
    public function cannotDeactivateUser(WebGuy $I)
    {
        $I->amLoggedAs([
            'email' => 'member_admin@email.com',
            'password' => 'asdfasdf'
        ]);
        $I->amOnPage('/users/59/edit');
        $I->wait(2);
        $I->see('Edit User');
        $I->click('Deactivate');
        $I->see('You can\'t Deactivate your own Account');

    }

    /**
     * 
     */
    public function deletesUserSuccessfully(WebGuy $I)
    {
        $I->amLoggedAs([
            'email' => 'member_admin@email.com',
            'password' => 'asdfasdf'
        ]);
        $I->amOnPage('/users/22/edit');
        $I->wait(2);
        $I->see('Edit User');
        $I->click('Delete');
        $I->see('User has been deleted!');

    }

    /**
     * 
     */
    public function deactivatesUserSuccessfully(WebGuy $I)
    {
        $I->amLoggedAs([
            'email' => 'member_admin@email.com',
            'password' => 'asdfasdf'
        ]);
        $I->amOnPage('/users/23/edit');
        $I->wait(2);
        $I->see('Edit User');
        $I->click('Deactivate');
        $I->see('User Status Updated');

    }

/**
 * * * * * * * * Portal Owner Tests
 */

    /**
     * create an user for this portal
     */
    public function createUser(WebGuy $I)
    {
        $I->amLoggedAs([
            'email' => 'owner@email.com',
            'password' => 'asdfasdf'
        ]);
        $I->see('Dashboard');
        $I->click('Users');
        $I->click('Create User');
        $I->fillField(['name' => 'name'], 'Cathy');
        $I->fillField(['name' => 'email'], 'cathy@company.com');
        $I->executeJS('return $("#account_id")[0].selectize.setValue("7");');
        $I->fillField(['name' => 'description'], 'Integer posuere erat a ante venenatis dapibus posuere velit aliquet.');
        $I->wait(2);
        $I->click('Save');
        $I->wait(2);
        $I->see('User has been Invited!');

        $I->seeInLastEmail('Cathy');
    }

    /**
     * @depends createUser
     */
    public function editUser(WebGuy $I)
    {
        $I->amLoggedAs([
            'email' => 'owner@email.com',
            'password' => 'asdfasdf'
        ]);
        $I->see('Dashboard');
        $I->click('Users');
        $I->click('Actions');
        $I->click('Edit');
        $I->wait(2);
        $I->fillField(['name' => 'name'], 'Smelly');
        $I->fillField(['name' => 'email'], 'pu@company.com');
        $I->executeJS('return $("#role")[0].selectize.setValue("7");');
        $I->fillField(['name' => 'description'], 'Integer posuere erat a ante venenatis dapibus posuere velit aliquet.');
        $I->click('Save changes');
        $I->wait(2);
        $I->see('User has been Updated!');
    }

    /**
     * @depends editUser
     */
    public function editUserNotes(WebGuy $I)
    {
        $I->amLoggedAs([
            'email' => 'owner@email.com',
            'password' => 'asdfasdf'
        ]);
        $I->see('Dashboard');
        $I->click('Users');
        $I->click('Actions');
        $I->click('Edit');
        $I->click('Notes');
        $I->wait(2);
        $I->see('Internal User Notes');
        $I->seeElement('.jumbotron');
        $I->fillField(['id' => 'notes_text'], 'Emeralds');
        $I->click('Save Note');
        $I->wait(2);
        $I->dontSeeElement('.jumbotron');
        $I->see('Emeralds');
    }

    /**
     * @depends createUser
     */
    public function deleteUser(WebGuy $I)
    {
        $I->amLoggedAs([
            'email' => 'owner@email.com',
            'password' => 'asdfasdf'
        ]);
        $I->see('Dashboard');
        $I->click('Users');
        $I->click('Actions');
        $I->click('Edit');
        $I->wait(2);
        $I->see('Edit User');
        $I->click('Delete');
        $I->see('User has been deleted!');

    }
}
