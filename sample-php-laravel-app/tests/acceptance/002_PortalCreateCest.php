<?php
use \WebGuy;

class PortalCreateCest
{
    public function _cleanup()
    {
        $this->getModule('Db')->_reconfigure(array('cleanup' => 'true'));
    }

    public function _before(WebGuy $I)
    {
        $I->amOnPage('/');   
    }

    public function _after(WebGuy $I)
    {
    }

    /**
     * create portal test
     */
    public function createPortal(WebGuy $I)
    {
        $I->see('Dashspace');
        $I->see('Create an account');
        $I->click('Create an account');
        $I->wait(1);
        $I->see('Open a DashSpace Account');
        $I->fillField(['name' => 'name'], 'Test Name');
        $I->fillField(['name' => 'email'], 'portalcreate@email.com');
        $I->fillField(['name' => 'password'], 'asdfasdf');
        $I->fillField(['name' => 'password_confirmation'], 'asdfasdf');
        $I->executeJS('$(".icheckbox_square-green").addClass("checked")');
        $I->executeJS('$("#terms").prop("checked", true)');

        $I->click('Register');
        $I->see('Dashboard');

        // TODO event UserCreatesAccount test

        $I->seeInLastEmail("Welcome to Dashspace");

        $I->click('//*[@id="page-wrapper"]/div[3]/div/div/div/div/div[1]/a/img');
        $I->wait(1);
        $I->see('Create a Portal');
        $I->fillField(['name' => 'name'], 'Test Portal');
        $I->fillField(['name' => 'portal_email'], 'portalcreate@email.com');
        $I->fillField(['name' => 'namespace'], 'portalcreate');
        $I->fillField(['name' => 'description'], 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.');
        $I->click('Save changes');
        $I->see('Your Portal has been Created!');

        // see sidebar icons
        $I->see('Spaces');
        $I->see('Accounts');
        $I->see('Users');

        // TODO event UserCreatesPortal tests
        // TODO PortalUserAccountAssignment tests
        // TODO InitializesDashboard tests

    }

    /**
     * 
     * @depends createPortal
     */
    public function editPortal(WebGuy $I)
    {
        $I->amLoggedAs([
            'email' => 'portalcreate@email.com',
            'password' => 'asdfasdf'
        ]);

        $I->see('Dashboard');
        $I->click('.caret');
        $I->wait(1);
        $I->see('Portal Settings');
        $I->click('Portal Settings');
        $I->wait(1);
        $I->see('Edit Portal');
        $I->fillField(['id' => 'name'], 'WeWork');
        $I->fillField(['id' => 'namespace'], 'wework');
        $I->fillField(['id' => 'portal_email'], 'portalcreate@email.com');

        $I->click('Save changes');
        $I->see('Your Portal has been Updated!');

    }
}
