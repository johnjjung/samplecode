<?php

class DailyRecurringInvoiceCest
{

    public function _before(FunctionalTester $I)
    {
        // onetime insert query
        // $I->haveInDatabase('invoices', ["id" => 1, "name" => "One Time", "create_days_before" => 3, "terms" => "5", "memo" => "", "line_items" => '', "meta" => "", "balance" => null, "customer_id" => 1, "portal_id" => 1, "account_id" => 6, "user_id" => 27, "created_at" => "2016-02-03 02:03:37", "updated_at" => "2016-02-03 02:03:37", "deleted_at" => null, "is_active" => 1, "is_email" => 1, "recurring_type" => "onetime", "interval" => "", "every" => "1", "on" => null, "start_at" => "2016-02-09 01:00:00", "end_at" => null, "occurences" => 0, ]);
    }

    public function _after(FunctionalTester $I)
    {
    }

    public function testsPortalIdNone(FunctionalTester $I)
    {
        Carbon::setTestNow(Carbon::parse('2016-02-08 23:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getDailyInvoices(2);
        // only 3 because one of the four is every other
        $I->assertTrue($recurring->count() == 0); 
        
    }

    public function testsPortalId(FunctionalTester $I)
    {
        Carbon::setTestNow(Carbon::parse('2016-02-08 23:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getDailyInvoices(1);
        // only 3 because one of the four is every other
        $I->assertTrue($recurring->count() == 3); 
        
    }

    // tests
    public function checkDailyTimeBeforeStartDate(FunctionalTester $I)
    {
        // before any of them start
        Carbon::setTestNow(Carbon::parse('2016-01-01 14:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getDailyInvoices();
        $I->assertEmpty($recurring);
    }

    public function checkDailyTimeRightBeforeStartDate(FunctionalTester $I)
    {
        // one hour before any of them start
        Carbon::setTestNow(Carbon::parse('2016-02-07 22:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getDailyInvoices();
        $I->assertEmpty($recurring);
    }

    // trigger first events
    public function checkDailyTimeRightOnStartDate(FunctionalTester $I)
    {
        // exact time when they start
        Carbon::setTestNow(Carbon::parse('2016-02-07 23:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getDailyInvoices();
        $I->assertTrue($recurring->count() == 4);
    }

    // next hour it triggers no events
    public function checkDailyTimeRightAfterStartDate(FunctionalTester $I)
    {
        // one hour after they start
        Carbon::setTestNow(Carbon::parse('2016-02-08 00:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getDailyInvoices();
        $I->assertEmpty($recurring);
    }

    // triggers second daily event
    public function checkDailyTimeSecondEvents(FunctionalTester $I)
    {
        // exact time when they start
        Carbon::setTestNow(Carbon::parse('2016-02-08 23:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getDailyInvoices();
        // only 3 because one of the four is every other
        $I->assertTrue($recurring->count() == 3);
    }

    // next hour it triggers no events
    public function checkDailyAfterSecondEvent(FunctionalTester $I)
    {
        // one hour after they start
        Carbon::setTestNow(Carbon::parse('2016-02-08 13:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getDailyInvoices();
        $I->assertTrue($recurring->count() == 0);
    }

    // triggers third daily event
    public function checkDailyThirdEvents(FunctionalTester $I)
    {
        // exact time when they start
        Carbon::setTestNow(Carbon::parse('2016-02-09 23:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getDailyInvoices();
        // should be four because it's the every other
        $I->assertTrue($recurring->count() == 4);
    }

    // next hour it triggers no events
    public function checkDailyAfterThirdEvent(FunctionalTester $I)
    {
        // one hour after they start
        Carbon::setTestNow(Carbon::parse('2016-02-10 00:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getDailyInvoices();
        $I->assertTrue($recurring->count() == 0);
    }


    // triggers fourth daily event
    public function checkDailyFourthEvents(FunctionalTester $I)
    {
        // exact time when they start
        Carbon::setTestNow(Carbon::parse('2016-02-10 23:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getDailyInvoices();
        // one of the 3 occurences is up
        $I->assertTrue($recurring->count() == 2);
    }

    // triggers fifth daily event
    public function checkDailyFifthEvents(FunctionalTester $I)
    {
        // exact time when they start
        Carbon::setTestNow(Carbon::parse('2016-02-11 23:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getDailyInvoices();
        // one of the 3rd occurences is up
        // one of the 3rd occurences is being used
        // two of them are not yet expired
        $I->assertTrue($recurring->count() == 3);
    }

    // triggers sixth daily event
    public function checkDailySixthEvents(FunctionalTester $I)
    {
        // exact time when they start
        Carbon::setTestNow(Carbon::parse('2016-02-12 23:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getDailyInvoices();
        // two of the 3rd occurences is up
        // two of them are not yet expired
        $I->assertTrue($recurring->count() == 2);
    }

    // triggers Seventh daily event
    public function checkDailySeventhEvents(FunctionalTester $I)
    {
        // exact time when they start
        Carbon::setTestNow(Carbon::parse('2016-02-13 23:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getDailyInvoices();
        // two of the 3rd occurences is up
        // one of them just expired
        
        $I->assertTrue($recurring->count() == 1);
    }

    // triggers Eighth daily event
    public function checkDailyEighthEvents(FunctionalTester $I)
    {
        // exact time when they start
        Carbon::setTestNow(Carbon::parse('2016-02-14 23:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getDailyInvoices();
        // two of the 3rd occurences is up
        // one of them just expired
        
        $I->assertTrue($recurring->count() == 1);
    }

    // triggers testing forever daily event
    public function checkDailyFarLaterEvents(FunctionalTester $I)
    {
        // exact time when they start
        Carbon::setTestNow(Carbon::parse('2018-02-14 23:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getDailyInvoices();
        // all but one expired
        
        $I->assertTrue($recurring->count() == 1);
    }

    // does a full 24 hour check
    public function checkFull24Hours(FunctionalTester $I)
    {
        for ($i=0; $i < 24; $i++) {
            // exact time when they start
            Carbon::setTestNow(Carbon::parse('2016-02-14 '.$i.':00:00'));
            $recurring = \App\Recurring\RecurInvoice::getDailyInvoices();
            // two of the 3rd occurences is up
            // one of them just expired
            if ($i != 23)
            {
                $I->assertTrue($recurring->count() == 0);
            }
            else
            {
                $I->assertTrue($recurring->count() == 1);   
            }
        }
        
    }

    // does a full 24 hour check with offset
    public function checkFull24HoursOffsetPlus(FunctionalTester $I)
    {
        for ($i=0; $i < 24; $i++) {

            // exact time when they start
            Carbon::setTestNow(Carbon::parse('2016-02-14 '.$i.':14:00'));
            $recurring = \App\Recurring\RecurInvoice::getDailyInvoices();
            // two of the 3rd occurences is up
            // one of them just expired
            if ($i != 23)
            {
                $I->assertTrue($recurring->count() == 0);
            }
            else
            {
                $I->assertTrue($recurring->count() == 1);   
            }
        }
        
    }

    // does a full month check
    public function checksAFullMonth(FunctionalTester $I)
    {
        for ($i=0; $i < 30; $i++) {

            // exact time when they start
            Carbon::setTestNow(Carbon::parse('2016-03-'.$i.' 23:00:00'));
            $recurring = \App\Recurring\RecurInvoice::getDailyInvoices();
            $I->assertTrue($recurring->count() == 1);   
        }
        
    }


    // TODO test in_actives

}
