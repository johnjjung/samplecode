<?php


class MonthlyRecurringInvoiceCest
{
    public function _before(FunctionalTester $I)
    {
    }

    public function _after(FunctionalTester $I)
    {
    }

    public function portalExists(FunctionalTester $I)
    {
        // exact time when they start
        Carbon::setTestNow(Carbon::parse('2016-03-01 05:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getMonthlyInvoices(1);
        $I->assertNotEmpty($recurring->count() == 1);
    }

    public function portalDoesntExist(FunctionalTester $I)
    {
        // exact time when they start
        Carbon::setTestNow(Carbon::parse('2016-03-01 05:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getMonthlyInvoices(2);
        $I->assertNotEmpty($recurring->count() == 0);
    }

    
    // tests
    public function checkTimeBeforeStartDate(FunctionalTester $I)
    {
        // before any of them start
        Carbon::setTestNow(Carbon::parse('2016-01-01 14:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getMonthlyInvoices();
        $I->assertEmpty($recurring);
    }

    public function checkTimeRightBeforeStartDate(FunctionalTester $I)
    {
        // one hour before any of them start
        Carbon::setTestNow(Carbon::parse('2016-02-05 04:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getMonthlyInvoices();
        $I->assertEmpty($recurring);
    }

    // trigger start date but no first event
    public function triggerStartWithNoEvent(FunctionalTester $I)
    {
        // exact time when they start
        Carbon::setTestNow(Carbon::parse('2016-02-05 05:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getMonthlyInvoices();
        $I->assertNotEmpty($recurring->count() == 0);
    }

    // trigger first event
    public function triggerFirstEvent(FunctionalTester $I)
    {
        // exact time when they start
        Carbon::setTestNow(Carbon::parse('2016-03-01 05:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getMonthlyInvoices();
        $I->assertNotEmpty($recurring->count() == 1);
    }


    // does a full 24 hour check
    public function checkFull24Hours(FunctionalTester $I)
    {
        for ($i=0; $i < 24; $i++) {

            // exact time when they start
            Carbon::setTestNow(Carbon::parse('2016-03-01 '.$i.':00:00'));
            $recurring = \App\Recurring\RecurInvoice::getMonthlyInvoices();
            // two of the 3rd occurences is up
            // one of them just expired
            if ($i != 05)
            {
                $I->assertTrue($recurring->count() == 0);
            }
            else
            {
                $I->assertTrue($recurring->count() == 1);   
            }
        }
        
    }

    // does a full 24 hour check
    public function checkFull24HoursWithOffset(FunctionalTester $I)
    {
        for ($i=0; $i < 24; $i++) {

            // exact time when they start
            Carbon::setTestNow(Carbon::parse('2016-03-01 '.$i.':00:00'));
            $recurring = \App\Recurring\RecurInvoice::getMonthlyInvoices();
            // two of the 3rd occurences is up
            // one of them just expired
            if ($i != 05)
            {
                $I->assertTrue($recurring->count() == 0);
            }
            else
            {
                $I->assertTrue($recurring->count() == 1);   
            }
        }
        
    }

    // trigger misses for 30 days
    public function trigger30DayMisses(FunctionalTester $I)
    {
        for ($i=2; $i < 30; $i++) {

            // exact time when they start
            Carbon::setTestNow(Carbon::parse('2016-03-'.$i.' 05:00:00'));
            $recurring = \App\Recurring\RecurInvoice::getMonthlyInvoices();
            $I->assertTrue($recurring->count() == 0);
        }
    }

    // trigger second event
    public function triggerSecondEvent(FunctionalTester $I)
    {
        // trigger miss
        Carbon::setTestNow(Carbon::parse('2016-03-31 01:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getMonthlyInvoices();
        $I->assertNotEmpty($recurring->count() == 0);

        // exact time when they start
        Carbon::setTestNow(Carbon::parse('2016-03-31 00:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getMonthlyInvoices();
        $I->assertNotEmpty($recurring->count() == 1);

        // trigger miss
        Carbon::setTestNow(Carbon::parse('2016-03-31 05:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getMonthlyInvoices();
        $I->assertNotEmpty($recurring->count() == 0);
    }

    // trigger third event
    public function triggerThirdEvent(FunctionalTester $I)
    {
        // trigger miss
        Carbon::setTestNow(Carbon::parse('2016-04-07 23:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getMonthlyInvoices();
        $I->assertNotEmpty($recurring->count() == 0);

        // exact time
        Carbon::setTestNow(Carbon::parse('2016-04-08 00:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getMonthlyInvoices();
        $I->assertNotEmpty($recurring->count() == 1);

        // trigger miss
        Carbon::setTestNow(Carbon::parse('2016-04-08 05:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getMonthlyInvoices();
        $I->assertNotEmpty($recurring->count() == 0);
    }

    // trigger every other month
    public function testEveryOtherMonth(FunctionalTester $I)
    {
        // trigger miss
        Carbon::setTestNow(Carbon::parse('2016-05-08 00:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getMonthlyInvoices();
        $I->assertNotEmpty($recurring->count() == 0);

        // exact time
        Carbon::setTestNow(Carbon::parse('2016-06-08 00:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getMonthlyInvoices();
        $I->assertNotEmpty($recurring->count() == 1);

        // trigger miss
        Carbon::setTestNow(Carbon::parse('2016-07-08 00:00:00'));
        $recurring = \App\Recurring\RecurInvoice::getMonthlyInvoices();
        $I->assertNotEmpty($recurring->count() == 0);
    }

    // TODO test occurences
    // TODO test inactive


}
