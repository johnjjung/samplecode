var gulp = require('gulp');
var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

// elixir(function(mix) {
//     mix.sass('app.scss');
// });


var spawn = require('child_process').spawn;
gulp.task('auto-reload', function() {
	var process;

	function restart() {
		if (process) {
			process.kill();
		}

		process = spawn('gulp', ['default'], {stdio: 'inherit'});
	}

	gulp.watch('gulpfile.js', restart);
	restart();
});
  
/**
 * Default gulp is to run this elixir stuff
 */
elixir(function(mix) {

	// Combine scripts
	mix.scripts([
	    'bower_components/jquery/dist/jquery.min.js',
	    'bower_components/bootstrap/dist/js/bootstrap.min.js',
	    'bower_components/moment/min/moment.min.js',
	    'bower_components/datetimepicker/jquery.datetimepicker.js',
	    'bower_components/selectize/dist/js/standalone/selectize.min.js',
	    'bower_components/toastr/toastr.min.js',
	    'bower_components/jt.timepicker/jquery.timepicker.min.js',
	    'bower_components/slimScroll/jquery.slimscroll.min.js',
	    'bower_components/switchery/dist/switchery.min.js',
	    'bower_components/underscore/underscore-min.js',
		'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
		'bower_components/fontawesome-iconpicker/dist/js/fontawesome-iconpicker.min.js',
		'bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js',
		'bower_components/dropzone/dist/min/dropzone.min.js',
		'public/js/plugins/metisMenu/jquery.metisMenu.js',
		'public/js/plugins/colorpicker/bootstrap-colorpicker.min.js',
		'public/js/plugins/iCheck/icheck.min.js',
		'public/js/inspinia.js',
		'public/js/app/main.js',
		'public/js/plugins/pace/pace.min.js',
	],
		'public/assets/lib.js',
		'./'
	);

	// Combine fonts
	mix.copy('./bower_components/font-awesome/fonts/', 'public/fonts/');

	// Combine css
	mix.styles([
		'bower_components/font-awesome/css/font-awesome.min.css',
		'bower_components/bootstrap/dist/css/bootstrap.min.css',
		'bower_components/selectize/dist/css/selectize.bootstrap3.css',
		'bower_components/selectize/dist/css/selectize.default.css',
	    'bower_components/toastr/toastr.min.css',
	    'bower_components/switchery/dist/switchery.min.css',
	    'bower_components/jt.timepicker/jquery.timepicker.css',
	    'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.standalone.min.css',
	    'bower_components/fontawesome-iconpicker/dist/css/fontawesome-iconpicker.min.css',
	    'bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css',
	    'bower_components/dropzone/dist/dropzone.css',
	    'bower_components/datetimepicker/jquery.datetimepicker.css',
	    'public/css/plugins/steps/jquery.steps.css',
	    'public/css/plugins/colorpicker/bootstrap-colorpicker.min.css',
	    'public/css/plugins/iCheck/custom.css',
	    'public/css/animate.css',
	],
		'public/assets/lib.css',
		'./'
	);

	// bower libs
	mix.copy('./bower_components/ion.rangeSlider/', 'public/assets/ion.rangeSlider/');
	mix.copy('./bower_components/fullcalendar/dist', 'public/assets/fullcalendar/');
	mix.copy('./bower_components/vue', 'public/assets/vue');

  // Compile Less
  // mix.less('admin.less', 'public/assets/css/admin.css');
});



