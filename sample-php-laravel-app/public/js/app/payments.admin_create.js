var payment_method;
$(document).ready(function () {
    // choose an account to continue
    $('#select_account').change(function() {
        location.href = "?account_id=" + $(this).val();
    });

    $('#select_invoice').change(function() {
        var link_build = window.location.pathname + "?account_id=" + $('#select_account').val() + "&invoice_id=" + $(this).val();
        console.log(link_build);
        location.href = link_build;
    });

    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

    // Review payment
    $('.calculate-button').click(function() {
        $(this).hide();
        var amount = $('#amount').val();
        $('.subtotal').load(window.location.pathname + window.location.search+'&amount='+amount+'&payment_method='+payment_method)
        $('.submit-button').show();
    });

    // test fill
    $('#pre-fill-bank').click(function() {
        $('#bank_account_type').val('PERSONAL_CHECKING');
        $('#bank_holder_name').val('John Jung');
        $('#bank_routing').val('490000018');
        $('#bank_account').val('35834534');
        $('#bank_phone').val('5555555555');
    });
    $('#pre-fill-cc').click(function() {
        $('#card_name').val('Test User');
        $('#card_number').val('4111111111111111');
        $('#card_exp_month').val('02');
        $('#card_exp_year').val('2026');
        $('#card_cvc').val('123');
        $('#card_zipcode').val('10001');
    });

    // quickbooks creditcard tokenization
    $('#bank_form').submit(function() {

        var payload = $(this).serializeObject();
        var qbPayload = {};
        qbPayload['bankAccount'] = {};

        // have to map everything cuz php sucks
        qbPayload['bankAccount']['name'] = payload['bank_holder_name'];
        qbPayload['bankAccount']['accountType'] = payload['bank_account_type'];
        qbPayload['bankAccount']['routingNumber'] = payload['bank_routing'];
        qbPayload['bankAccount']['accountNumber'] = payload['bank_account'];
        qbPayload['bankAccount']['phone'] = payload['bank_phone'];

        // Call QB to tokenize
        intuit.ipp.payments.tokenize(
            "{!! env('QB_APP_TOKEN') !!}",
            qbPayload,
            function(token, response) {
                if (token != null) {
                    console.log(token);
                    $('.token').val(token);
                    // process 
                    $.post('/admin/payment_accounts', $('#bank_form').serialize(), function(r) {
                        console.log(r);
                        $('#add_bank_account_modal').modal('hide');
                    });
                    // reload the list of credit card
                    $('.bank_list').load('/admin/payment_accounts/method/bank_account?account_id={!! Request::query("account_id") !!}', function() {
                            $('.calculate-button').show();
                    });
                } else {
                    console.log(response.message);
                    $('#bank_form').show();
                    $('.msg').empty();
                    $('.msg').append(response.message);
                }
        });

        return false;

    });


    // quickbooks creditcard tokenization
    $('#credit_card_form').submit(function() {

        var payload = $(this).serializeObject();
        var qbPayload = {};
        qbPayload['card'] = {};
        qbPayload['card']['address'] = {};

        // have to map everything cuz php sucks
        qbPayload['card']['name'] = payload['card_name'];
        qbPayload['card']['number'] = payload['card_number'];
        qbPayload['card']['cvc'] = payload['card_cvc'];
        qbPayload['card']['expMonth'] = payload['card_exp_month'];
        qbPayload['card']['expYear'] = payload['card_exp_year'];
        qbPayload['card']['address']['postalCode'] = payload['card_zipcode'];

        // Call QB to tokenize
        intuit.ipp.payments.tokenize(
            "{!! env('QB_APP_TOKEN') !!}",
            qbPayload,
            function(token, response) {
                if (token != null) {
                    $('.token').val(token);
                    // process credit card
                    $.post('/admin/payment_accounts', $('#credit_card_form').serialize(), function(r) {
                        $('#add_credit_card_modal').modal('hide');
                    });
                    // reload the list of credit card
                    $('.card_list').load('/admin/payment_accounts/method/credit_card?account_id={!! Request::query("account_id") !!}', function() {
                        $('.calculate-button').show();
                    });
                } else {
                    $('#credit_card_form').show();
                    console.log(response);
                    $('.msg').empty();
                    $('.msg').append(response)
                }
        });

        return false;

    });

    // clicking radio payment method activates form
    $('.method_type').on('ifClicked' ,function() {
        $('.amount_form').show();
        var method_type = (this.value);
        payment_method = (this.value);
        if (method_type == 'credit_card')
        {
            $('.card_list').show();
            $('.bank_list').hide();
            $('.card_list').load('/admin/payment_accounts/method/credit_card', function() {
                $('.calculate-button').show();
            });
        }
        else
        {
            $('.card_list').hide();
            $('.bank_list').show();
            $('.bank_list').load('/admin/payment_accounts/method/bank_account', function() {
                $('.calculate-button').show();
            });
        }

    });

    $('#amount').keyup(function() {
        $('.submit-button').hide();
        $('.calculate-button').show();
    });

});