$(document).ready(function() {
	/**
	 * initializes colorpicker forms
	 */
	$('.colorpicker').colorpicker();

	/**
	 * initializes datepicker forms
	 */
	$('.datepicker').datepicker();

	/**
	 * initializes datetimepicker
	 */
	$('.datetimepicker').datetimepicker();


	/**
	 * initializes iconpicker forms
	 */
	$('.iconpicker').iconpicker({
		placement: 'bottomLeft'
	});

	/**
	 * Selectize initialize
	 */
	 $('.selectize').selectize();

	 /**
	  * Initializes tooltip
	  */
	 $('.tthover').tooltip();

	 /**
	  * Intialize icheck
	  */
	 $('.i-checks').iCheck({
	     checkboxClass: 'icheckbox_square-green',
	     radioClass: 'iradio_square-green',
	 });

	/**
	 * initializes clear button
	 */
	$('.clearform').click(function(){
	    $('#clearform').append('<input type="hidden" name="'+$(this).data('input')+'" value="" />');
	    $('#clearform').submit();
	});

	/**
	 * initializes switchery js-switch class checkbox
	 */
	var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
	elems.forEach(function(html) {
	  var switchery = new Switchery(html);
	});

	/**
	 * Execute a JS function by name
	 * 
	 * @param  {string} functionName the name of the function to call
	 * @param  {context} context     the context of the function call
	 * @param  {array} args          the args to the function
	 */
	function executeFunctionByName(functionName, context /*, args */) {
		var args = [].slice.call(arguments).splice(2);
		var namespaces = functionName.split(".");
		var func = namespaces.pop();
		for(var i = 0; i < namespaces.length; i++) {
			context = context[namespaces[i]];
		}
		return context[func].apply(this, args);
	}

	/**
	 * serializes Object
	 * 
	 * https://github.com/hongymagic/jQuery.serializeObject
	 * @param  {Object} ){"use strict";var   a [description]
	 * @param  {[type]} b       [description]
	 * @param  {[type]} b       [description]
	 * @return {[type]}         [description]
	 */
	$.fn.serializeObject=function(){"use strict";var a={},b=function(b,c){var d=a[c.name];"undefined"!=typeof d&&d!==null?$.isArray(d)?d.push(c.value):a[c.name]=[d,c.value]:a[c.name]=c.value};return $.each(this.serializeArray(),b),a};



	/**
	 * AJAX LINKS
	 *
	 * Allows links to send AJAX requests by specifying data-* attributes
	 * Assign the class 'ajax-link' to the DOM and the following data-*
	 *
	 * @required data-url
	 * @optional data-method
	 * @optional data-onsuccess
	 */
	$(document).on('click', '.ajax-link', function() {
		console.log($(this).data('body'));
		$.ajax({
			method: $(this).data('method') ? $(this).data('method') : 'POST',
			url: $(this).data('url'),
			data: $(this).data('body') ? $(this).data('body') : null,
			context: this,
			success: function(response) {
				if ( $(this).data('onsuccess') ) {
					executeFunctionByName( $(this).data('onsuccess'), window, response);
				}
			}
		});
	});

});