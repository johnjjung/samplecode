
function commentCountTick(post_id, tick)
{
	// increment like count
	var commentCounter = $('.commentCount[data-post-id="'+post_id+'"]');
	var commentCount = parseInt(commentCounter.text());

	if (tick)
	{
		commentCount++;
	}
	else
	{
		commentCount--;
	}
	commentCounter.empty();
	commentCounter.append(commentCount);
}

// after liking a post
function postLiked(post_liked_id)
{
    // increment like count
    var likecounter = $('.likecount[data-post-id="'+post_liked_id+'"]');
    var likecount = parseInt(likecounter.text());
    likecount++;
    likecounter.empty();
    likecounter.append(likecount);

    // swap like button with unlike button
    $('.like-button[data-post-id="'+post_liked_id+'"]').hide();
    $('.unlike-button[data-post-id="'+post_liked_id+'"]').show();

}

// after liking a post
function postUnliked(post_liked_id)
{
    // increment like count
    var likecounter = $('.likecount[data-post-id="'+post_liked_id+'"]');
    var likecount = parseInt(likecounter.text());
    likecount--;
    likecounter.empty();
    likecounter.append(likecount);

    // swap like button with unlike button
    $('.like-button[data-post-id="'+post_liked_id+'"]').show();
    $('.unlike-button[data-post-id="'+post_liked_id+'"]').hide();

}


function Post()
{
	this.page = 2;
	this.getPosts = function(selector)
	{
		var page = this.page;
		$.get('/posts/?page='+page, function(r) {
		    page++;
		    selector.append(r);
		});
		this.page = page;
	};
	this.getUserPosts = function(selector, user_id)
	{
		var page = this.page;
		$.get('/users/'+user_id+'?page='+page, function(r) {
		    page++;
		    selector.append(r);
		});
		this.page = page;
	}

}

