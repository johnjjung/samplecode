// after liking a comment
function commentLiked(comment_liked_id)
{
    // increment like count
    var likecounter = $('.likecount[data-comment-id="'+comment_liked_id+'"]');
    var likecount = parseInt(likecounter.text());
    likecount++;
    likecounter.empty();
    likecounter.append(likecount);

    // swap like button with unlike button
    $('.like-button[data-comment-id="'+comment_liked_id+'"]').hide();
    $('.unlike-button[data-comment-id="'+comment_liked_id+'"]').show();

}

// after liking a comment
function commentUnliked(comment_liked_id)
{
    // increment like count
    var likecounter = $('.likecount[data-comment-id="'+comment_liked_id+'"]');
    var likecount = parseInt(likecounter.text());
    likecount--;
    likecounter.empty();
    likecounter.append(likecount);

    // swap like button with unlike button
    $('.like-button[data-comment-id="'+comment_liked_id+'"]').show();
    $('.unlike-button[data-comment-id="'+comment_liked_id+'"]').hide();

}
// remove comment
function removeComment(id)
{
    var post_id = $('.chat-message[data-comment-id="'+id+'"]').data('post-id');
    commentCountTick(post_id, false);
    $('.chat-message[data-comment-id="'+id+'"]').remove();


}

// gets and appends comments to post
function getComments(post_id)
{
    $.get('/comments/'+post_id+"/post", function(r) {
        $('.list-comments[data-post-id="'+post_id+'"]').append(r);
    });
}