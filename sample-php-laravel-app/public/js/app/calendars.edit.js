$('#timezone').selectize();

// var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
// elems.forEach(function(html) {
//   var switchery = new Switchery(html);
// });

function hourFromInput(input)
{
   return moment(input.val(), "HH:mm:ss").format("HH");
}

function momentFromInput(input)
{
    return moment(input.val(), "HH:mm:ss").subtract(1,'days').format("X");
}

// operating hours
$('#time_limit_slider').ionRangeSlider({
    type: "double",
    min: +moment().subtract(1,'days').startOf('day').format("X"),
    max: +moment().subtract(1,'days').startOf('day').format("X") + 3600*24,
    from: +momentFromInput($('#open_time')),
    to: +momentFromInput($('#close_time')),
    step: 60 * 30,
    grid: true,
    prettify: function (num) {
        var m = moment(num, "X").locale("en");
        return m.format("LT");
    },
    onChange: function (data) {
    	$('#open_time').val(moment(data.from, "X").format("HH:mm:ss"));
    	$('#close_time').val(moment(data.to, "X").format("HH:mm:ss"));
    }
});

// peak hours
$('#peak_hours_slider').ionRangeSlider({
    type: "double",
    min: +moment().subtract(1,'days').startOf('day').format("X"),
    max: +moment().subtract(1,'days').startOf('day').format("X") + 3600*24,
    from: +momentFromInput($('#peak_start')),
    to: +momentFromInput($('#peak_end')),
    step: 60 * 30,
    grid: true,
    prettify: function (num) {
        var m = moment(num, "X").locale("en");
        return m.format("LT");
    },
    onChange: function (data) {
    	$('#peak_start').val(moment(data.from, "X").format("HH:mm:ss"));
    	$('#peak_end').val(moment(data.to, "X").format("HH:mm:ss"));
    }
});

// max time limit
$('#max_time_limit_slider').ionRangeSlider({
    type: "single",
    min: +moment().subtract(1,'days').startOf('day').format("X"),
    max: +moment().subtract(1,'days').startOf('day').format("X") + 3600*24 - 1,
    from: +momentFromInput($('#max_time_limit')),
    step: 60 * 30,
    grid: true,
    prettify: function (num) {
        var m = moment(num, "X").locale("en");
        return m.format("H") + " hours " + m.format("m") + " minutes";
    },
    onChange: function (data) {
    	var m = moment(data.from, "X").locale("en");
    	m = m.format("HH:mm:ss");
    	$('#max_time_limit').val(m);
    }

});


// min time limit
$('#min_time_limit_slider').ionRangeSlider({
    type: "single",
    min: +moment().subtract(1,'days').startOf('day').format("X"),
    max: +moment().subtract(1,'days').startOf('day').format("X") + 3600*24 - 1,
    from: +momentFromInput($('#min_time_limit')),
    step: 60 * 30,
    grid: true,
    prettify: function (num) {
        var m = moment(num, "X").locale("en");
        return m.format("H") + " hours " + m.format("m") + " minutes";
    },
    onChange: function (data) {
    	var m = moment(data.from, "X").locale("en");
    	m = m.format("HH:mm:ss");
        console.log(moment(data.from, "X"));
    	$('#min_time_limit').val(m);
    }
});
