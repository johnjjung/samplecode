$('#timezone').selectize();


// operating hours
$('#time_limit_slider').ionRangeSlider({
    type: "double",
    min: +moment().subtract(1,'days').startOf('day').format("X"),
    max: +moment().subtract(1,'days').startOf('day').format("X") + 3600*24,
    from: +moment().subtract(1,'days').startOf('day').format("X") + 3600*8,
    to: +moment().subtract(1,'days').startOf('day').format("X") + 3600*18,
    step: 60 * 30,
    grid: true,
    prettify: function (num) {
        var m = moment(num, "X").locale("en");
        return m.format("LT");
    },
    onChange: function (data) {
    	$('#open_time').val(moment(data.to, "X").format("HH:mm:ss"));
    	$('#close_time').val(moment(data.to, "X").format("HH:mm:ss"));
        // console.log(moment(data.to, "X").format("HH:mm:ss"));
        // console.log(moment(data.from, "X").format("HH:mm:ss"));
    },
    onStart: function (data)
    {
        $('#open_time').val(moment(data.from, "X").format("HH:mm:ss"));
        $('#close_time').val(moment(data.to, "X").format("HH:mm:ss"));
    }
});

// peak hours
$('#peak_hours_slider').ionRangeSlider({
    type: "double",
    min: +moment().subtract(1,'days').startOf('day').format("X"),
    max: +moment().subtract(1,'days').startOf('day').format("X") + 3600*24,
    from: +moment().subtract(1,'days').startOf('day').format("X") + 3600*10,
    to: +moment().subtract(1,'days').startOf('day').format("X") + 3600*14,
    step: 60 * 30,
    grid: true,
    prettify: function (num) {
        var m = moment(num, "X").locale("en");
        return m.format("LT");
    },
    onChange: function (data) {
    	$('#peak_start').val(moment(data.to, "X").format("HH:mm:ss"));
    	$('#peak_end').val(moment(data.to, "X").format("HH:mm:ss"));
        // console.log(moment(data.to, "X").format("HH:mm:ss"));
        // console.log(moment(data.from, "X").format("HH:mm:ss"));
    },
    onStart: function (data)
    {
        $('#peak_start').val(moment(data.from, "X").format("HH:mm:ss"));
        $('#peak_end').val(moment(data.to, "X").format("HH:mm:ss"));
    }
});

// max time limit
$('#max_time_limit_slider').ionRangeSlider({
    type: "single",
    min: +moment().subtract(1,'days').startOf('day').format("X"),
    max: +moment().subtract(1,'days').startOf('day').format("X") + 3600*24 - 1,
    from: +moment().subtract(1,'days').startOf('day').format("X") + 3600*4,
    step: 60*30,
    grid: true,
    prettify: function (num) {
        var m = moment(num, "X").locale("en");
        return m.format("H") + " hours " + m.format("m") + " minutes";
    },
    onChange: function (data) {
    	// console.log(data.from);
    	var m = moment(data.from, "X").locale("en");
    	m = m.format("HH:mm:ss");

    	$('#max_time_limit').val(m);
        // console.log(m);
    },
    onStart: function (data)
    {
        $('#max_time_limit').val(moment(data.to, "X").locale("en").format("HH:mm:ss"));
    }
});


// min time limit
$('#min_time_limit_slider').ionRangeSlider({
    type: "single",
    min: +moment().subtract(1,'days').startOf('day').format("X"),
    max: +moment().subtract(1,'days').startOf('day').format("X") + 3600*24 - 1,
    from: +moment().subtract(1,'days').startOf('day').format("X") + 1800,
    step: 60*30,
    grid: true,
    prettify: function (num) {
        var m = moment(num, "X").locale("en");
        return m.format("H") + " hours " + m.format("m") + " minutes";
    },
    onChange: function (data) {
    	// console.log(data.from);
    	var m = moment(data.from, "X").locale("en");
    	m = m.format("HH:mm:ss");

    	$('#min_time_limit').val(m);
        // console.log(m);
    },
    onStart: function (data)
    {
        $('#min_time_limit').val(moment(data.from, "X").locale("en").format("HH:mm:ss"));
    }
});

// initialize val on create form

