/**
 * initializes compose modal
 */
 function composeInitialize()
 {
 	$('#compose').click(function() {
 		$('#compose-modal').modal();
 	});
 }
 
 composeInitialize();