// https://brianreavis.github.io/selectize.js/
$('#tag_text').selectize({
	plugins: ['remove_button'],
	selectOnTab: true,
	persist: false,
	create: function(input) {
        return {
            value: input,
            text: input
        }
    }
});
$('#state').selectize();
$('#select_account').selectize();