function modalInit(reservable_object)
{
	var start_hour = reservable_object.data('step');
	var start_time = moment(start_hour, "H").format("H:mma");
	var end_calendars = moment('00:30:00', "H").format("H:mma");
	$('#reservation_date').val($('#chosen_date').val());

	// show calendar date
	$('.reservation-date').empty();
	$('.reservation-date').append(moment($('#chosen_date').val()).format("MMM Do YYYY"));

	// show calendar name
	$('.reservation-calendar-name').empty();
	if (reservable_object.parent().data('calendar-name') == undefined)
	{
		$('.reservation-calendar-name').append(reservable_object.data('calendar-name'));
		$('#calendar_id').val(reservable_object.data('calendar_id'));
	}
	else
	{
		$('.reservation-calendar-name').append(reservable_object.parent().data('calendar-name'));
		$('#calendar_id').val(reservable_object.parent().data('calendar_id'));
	}

	// $('.reservation-date').append($('#chosen_date').val());	

	$('.reserve').modal();
	$('#start_time').val(start_time);
	$('#start_time').timepicker({
	    'timeFormat': 'h:i a'
	}).on('change', function() {
		$('#end_time').timepicker('remove');
		$('#end_time').timepicker({
		    'minTime': $('#start_time').val(),
		    'maxTime': end_calendars,
		    'step': 15,
		    'showDuration': true,
		    'timeFormat': 'h:i a'
		});
	});

	$('#end_time').timepicker('remove');
	$('#end_time').timepicker({
	    'minTime': start_time,
	    'maxTime': end_calendars,
	    'step': 15,
	    'showDuration': true,
	    'timeFormat': 'h:i a'
	});
}

function modalCreateInit(reservable_object)
{
	modalInit(reservable_object);

	$('#createReservationForm').submit(function() {
		$.ajax({
			method: "POST",
			url: "/admin/reservations",
			data: $(this).serialize(),
			success: function(d) {
				// refresh calendars and clear forms
				$('#refresh').click();
				$('.reserve').modal('hide');
				toastr['success']("Reservation Created");
			},
			error: function(e)
			{	
				_.each(e.responseJSON, function(error)
				{
					toastr['error'](error);
				});
			}
		});
	});
}

function modalEditInit(reservable_object)
{
	modalInit(reservable_object);

	$('#editReservationForm').submit(function() {
		$.ajax({
			method: "PUT",
			url: "/admin/reservations/"+reservable_object.data('reservation-id'),
			data: $(this).serialize(),
			success: function(d) {
				// refresh calendars and clear forms
				$('#refresh').click();
				$('.reserve').modal('hide');
				toastr['success']("Reservation Updated");
			},
			error: function(e)
			{	
				_.each(e.responseJSON, function(error)
				{
					toastr['error'](error);
				});
			}
		});
	});

	$('#deleteReservationForm').submit(function() {
		$.ajax({
			method: "DELETE",
			url: "/admin/reservations/"+reservable_object.data('reservation-id'),
			data: $(this).serialize(),
			success: function(d) {
				// refresh calendars and clear forms
				$('#refresh').click();
				$('.reserve').modal('hide');
				toastr['success']("Reservation Deleted");
			}
		});
	});
}

function modalCreate(reservable_object)
{
	$('.modal-content').empty();
	$('.modal-content').load('/admin/reservations/create',
		function(d) {
			$('.modal-content').empty();
			$('.modal-content').append(d);
			modalInit(reservable_object)
			modalCreateInit(reservable_object);
		});
}

function modalEdit(reservable_object)
{
	$('.modal-content').empty();
	$('.modal-content').load('/admin/reservations/'+reservable_object.data('reservation-id')+'/edit',
		function(d) {
			$('.modal-content').empty();
			$('.modal-content').append(d);
			modalEditInit(reservable_object);
		});
}

function modalShow(reservable_object)
{
	$('.modal-content').empty();
	$('.modal-content').load('/admin/reservations/'+reservable_object.data('reservation-id'),
		function(d) {
			$('.modal-content').empty();
			$('.modal-content').append(d);
			$('.reserve').modal();
		});
}

function datepickerInit()
{
	// initialize date picker
	if($('#datePicker').length > 0) {
		$('#datePicker').datepicker({
			// startDate: '0',
			todayHighlight: true
		}).on('changeDate', function(e){
			// on date change
	      	$('#chosen_date').val(e.format('yyyy-mm-dd'));
	      	$('#calendars').load('/admin/reservations/calendars?date='+$('#chosen_date').val(), null, function() {
	      		$('#reservation_date').val($('#chosen_date').val());
	      		calendarInit();
	      	});
    	});
    	// set pointers
		$('.datepicker .prev').html('<i class="fa fa-angle-left"></i>');
		$('.datepicker .next').html('<i class="fa fa-angle-right"></i>');
	}
}

function calendarInit()
{
	$('.reservable').click(function() {
		modalCreate($(this));
	});

	$('.ours').click(function() {
		modalEdit($(this));
	});

	$('.reserved').click(function() {
		modalShow($(this));
	});

	$('#reservation_date').val($('#chosen_date').val());
}


