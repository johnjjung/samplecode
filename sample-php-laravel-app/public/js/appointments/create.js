$(function() {

	$('.timeslot').click(function() {
		console.log('timeslot clicked');
		$('.timeslot').removeClass('active');
		$(this).addClass('active');
	});

	$('#appt_date_picker').datetimepicker({
		format: 'YYYY-MM-DD',
		inline: true,
		minDate: moment().startOf('day'),
	});
});