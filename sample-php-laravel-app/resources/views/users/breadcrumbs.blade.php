<ol class="breadcrumb">
    <li>
        <a href="/">Home</a>
    </li>
    <li>
        <a href="/users">
            Users
        </a>
    </li>
    @if(@$user->name)
    <li class="active">
        <strong>
            {!! $user->name !!}
        </strong>
    </li>
    @endif
</ol>