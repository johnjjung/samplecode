
<div class="btn-group">

	@if(!$user->hasRole(['portal_owner', 'member_owner', 'root']))
    <button data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle" aria-expanded="false">Reassign <span class="caret"></span></button>
    @endif

    <ul class="dropdown-menu">

	@if(empty($user->account_id))
    <li>
    	<a href="/admin/users/{!! $user->id !!}/edit">
    		Assign an Account
    	</a>
    </li>
	@endif
        

{{-- if i am a portal owner --}}
	{{-- everything --}}
{{-- if i am a portal admin --}}
	{{-- everything except make portal owner --}}
{{-- if i am a portal employee --}}
	{{-- only member controls --}}

@if(Auth::user()->hasRole(['portal_owner', 'portal_admin']))
	{{-- user is admin/employee and i am a portal owner --}}
	@if($user->hasRole(['portal_admin', 'portal_employee']) && Auth::user()->hasRole('portal_owner'))
		<li>
	    {{-- // make portal owner --}}
	    {!! Form::open(['url' => 'admin/users/role_update/' . $user->id]) !!}
	        {!! Form::hidden('_method', 'PUT') !!}
	        {!! Form::hidden('role', 'portal_owner') !!}
	        {!! Form::submit("Make Portal Owner", ['class' => 'btn btn-link']) !!}                    
	    {!! Form::close() !!}
	    </li>
	@endif

	{{-- user is a portal admin --}}
	@if($user->hasRole('portal_admin'))
		<li>
		{{-- // make portal employee --}}
		{!! Form::open(['url' => 'admin/users/role_update/' . $user->id]) !!}
		    {!! Form::hidden('_method', 'PUT') !!}
		    {!! Form::hidden('role', 'portal_employee') !!}
		    {!! Form::submit("Make Portal Employee", ['class' => 'btn btn-link']) !!}                    
		{!! Form::close() !!}
		</li>
	@endif

	{{-- user is a portal employee --}}
	@if($user->hasRole('portal_employee'))
	    {{-- make portal admin --}}
	    <li>
	    {!! Form::open(['url' => 'admin/users/role_update/' . $user->id]) !!}
	        {!! Form::hidden('_method', 'PUT') !!}
	        {!! Form::hidden('role', 'portal_admin') !!}
	        {!! Form::submit("Make Portal Admin", ['class' => 'btn btn-link']) !!}                    
	    {!! Form::close() !!}
	    </li>
	@endif
@endif


@if(Auth::user()->hasRole(['portal_owner', 'portal_admin', 'portal_employee', 'member_owner', 'member_admin']))
	@if($user->hasRole(['member_admin', 'member']) && (!Auth::user()->hasRole('member_admin')) && (!Auth::user()->hasRole('member')))
	    {{-- make member owner --}}
	    <li>
	    {!! Form::open(['url' => 'admin/users/role_update/' . $user->id]) !!}
	        {!! Form::hidden('_method', 'PUT') !!}
	        {!! Form::hidden('role', 'member_owner') !!}
	        {!! Form::submit("Make Member Owner", ['class' => 'btn btn-link']) !!}                    
	    {!! Form::close() !!}
	    </li>
	    {{-- make member member --}}
	@endif

	@if($user->hasRole('member_admin'))
		<li>
		{!! Form::open(['url' => 'admin/users/role_update/' . $user->id]) !!}
		    {!! Form::hidden('_method', 'PUT') !!}
		    {!! Form::hidden('role', 'member') !!}
		    {!! Form::submit("Make Member", ['class' => 'btn btn-link']) !!}                    
		{!! Form::close() !!}
		</li>
	@endif
	@if($user->hasRole('member'))
	    {{-- make member admin --}}
	    <li>
	    {!! Form::open(['url' => 'admin/users/role_update/' . $user->id]) !!}
	        {!! Form::hidden('_method', 'PUT') !!}
	        {!! Form::hidden('role', 'member_admin') !!}
	        {!! Form::submit("Make Member Admin", ['class' => 'btn btn-link']) !!}                    
	    {!! Form::close() !!}
	    </li>
	@endif
@endif


        
    </ul>
</div>
