{{-- portal owner account cannot be disabled --}}

@if(!$user->hasRole(['portal_owner']) )
	@if($user->is_active)
	{!! Form::open(['url' => ['/users/toggle', $user->id], 'style' => 'display:inline;']) !!}
	    {!! Form::hidden('_method', 'PUT') !!}
	    {!! Form::hidden('is_active', false) !!}
	    {!! Form::submit("Deactivate", ['class' => 'btn btn-xs btn-danger']) !!}                    
	{!! Form::close() !!}
	@else
	{!! Form::open(['url' => ['/users/toggle', $user->id], 'style' => 'display:inline;']) !!}
	    {!! Form::hidden('_method', 'PUT') !!}
	    {!! Form::hidden('is_active', true) !!}
	    {!! Form::submit("Activate", ['class' => 'btn btn-xs btn-info']) !!}                    
	{!! Form::close() !!}
	@endif
@endif