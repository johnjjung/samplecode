@extends('layouts.default')

@section('heading')
<h1>User Profile</h1>

@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInUp">
            <div class="row">
                {{-- user profile box --}}
                @include('users.profile')

                {{-- user activity box --}}
                <div class="col-md-9">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Activity</h5>
                        </div>
                        <div class="ibox-content" id="activity">
                            @include('posts.activity_list')
                        <div>
                    </div>
                </div>
            </div>
        </div>

        @include('users.message')
    </div>
</div>
@stop

@section('tail')
{!! HTML::script('/js/app/comments.js') !!}
{!! HTML::script('/js/app/posts.js') !!}

<script type="text/javascript">
    var posts = new Post();
    var user_id = {!! ($user->id) ?: null; !!}

    function neverending_posts()
    {
        $('.loadmore').click(function() {
            $(this).hide();
            posts.getUserPosts($('#activity'), user_id);
        });
    }
</script>
@endsection
