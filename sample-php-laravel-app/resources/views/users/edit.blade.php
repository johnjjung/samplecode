@extends('layouts.default')

@section('heading')
<h1>Users</h1>
@include('users.breadcrumbs')
@endsection

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">

            @include('users.nav')

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Edit User</h5>
                    <div class="ibox-tools">
                        @include('users.toggle_activation')
                    </div>
                </div>

                <div class="ibox-content">
                    <h2>Edit User</h2>
                    <p>A User is person who belongs to an Account to have access to the assigned Spaces.</p>
                    @include('partials.error_bar')
                    {!! Form::model(@$user, ['method' => 'PUT', 'url' => "/users/{$user->id}", 'class' => 'form-horizontal', 'files' => true]) !!}
                    <br>
                    {{-- account --}}
                    <div class="form-group">
                        {!! Form::label('name', 'User Full Name', ['class' => 'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('name', null, ['class' => 'form-control', 'required' => true]) !!}
                            <small class="text-danger">{{ $errors->first('name') }}</small>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('email', 'User Email', ['class' => 'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::text('email', null, ['class' => 'form-control', 'required' => true]) !!}
                            <small class="text-danger">{{ $errors->first('email') }}</small>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('profile_pic', 'User Profile Picture', ['class' => 'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">
                            <img src="{!! $user->profile_pic !!}">
                            <span class="help-block m-b-none">This is your profile picture. For best Results, picture should be a jpg, png, or gif file. Dimensions should be at least 300px x 300px. If you don't have a picture, the initials will be used.</span>
                            {!! Form::file('profile_pic', null, ['class' => 'form-control', 'required' => true]) !!}
                            <small class="text-danger">{{ $errors->first('profile_pic') }}</small>
                        </div>
                    </div>

                    {{-- TODO only if user is admin/owner --}}
                    <div class="form-group">
                        {!! Form::label('role', 'Member Type', ['class' => 'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::select('role', $roles, @$user->roles->lists('name')[0], ['class' => 'form-control selectize', 'id' => 'role']) !!}
                            <small class="text-danger">{{ $errors->first('role') }}</small>
                            <small>Admin's have more access and privileges than members.</small>
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('description', 'About', ['class' => 'col-sm-2 control-label']) !!}
                        <div class="col-sm-10">
                            {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                            <small class="text-danger">{{ $errors->first('description') }}</small>
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>
                    {!! Form::hidden('is_active', true) !!}
                    {!! Form::hidden('account_id', Auth::user()->account_id) !!}
                    {!! Form::hidden('portal_id', Auth::user()->portal_id) !!}

                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit">Save changes</button>
                            {!! Form::close() !!}
                            {!! Form::open(['route' => ['users.destroy', $user->id]]) !!}
                                {!! Form::hidden('_method', 'DELETE') !!}
                                {!! Form::submit("Delete", ['class' => 'btn btn-danger']) !!}                    
                            {!! Form::close() !!}
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('tail')

<!-- Steps -->
{!! HTML::script('js/plugins/staps/jquery.steps.min.js'); !!}

<!-- Jquery Validate -->
{!! HTML::script('js/plugins/validate/jquery.validate.min.js'); !!}

{!! HTML::script('js/app/users.edit.js') !!}

@endsection
