@extends('layouts.preauth')

@section('content')

<h3>Set Password</h3>
<form class="form-horizontal m-t" role="form" method="POST" action="{{ url('/users/invited/' . $token) }}">
	{!! csrf_field() !!}
	<div class="form-group">
		<label class="col-md-4 control-label">Password</label>
		<div class="col-md-8">
			<input type="password" class="form-control" name="password">
		</div>
	</div>

	<div class="form-group">
		<label class="col-md-4 control-label">Confirm Password</label>
		<div class="col-md-8">
			<input type="password" class="form-control" name="password_confirmation">
		</div>
	</div>

	<div class="form-group">
		<div class="col-md-8 col-md-offset-4">
			<button type="submit" class="btn btn-primary">
				Continue
			</button>
		</div>
	</div>
</form>
				
@endsection
