<h2>Internal User Notes</h2>
<p>This area is for Internal Portal Notes about this User.</p>
<p>All Portal Owners, Admins, and Employees will be able to see and write notes here. The User will not see these notes, unless they're part of the portal management team.</p>

{!! Form::model(@$note, ['method' => 'POST', 'route' => 'admin.notes.store', 'class' => 'form-horizontal', 'onsubmit' => 'return false;', 'id' => 'notes_form']) !!}
<br>
<div class="form-group">
    <div class="col-sm-12">
        {!! Form::textarea('text', null, ['class' => 'form-control', 'placeholder' => 'Add Notes', 'rows' => 3, 'id' => 'notes_text']) !!}
        {!! Form::hidden('notable_type', 'App\User') !!}
        {!! Form::hidden('notable_id', $user->id) !!}
        {!! Form::hidden('user_id', Auth::user()->id) !!}
        {!! Form::hidden('portal_id', Auth::user()->portal_id) !!}
        <br/>
        {!! Form::submit("Save Note", ['class' => 'btn btn-success']) !!}  
    </div>
</div>
{!! Form::close() !!}

<div class="hr-line-dashed"></div>

@if(!$notes->isEmpty())
<div class="feed-activity-list">
    @include('notes.list')
</div>
    @if($notes->count() > 19)
    <div class="">
        <button id="more_notes" class="btn btn-primary btn-block m">
            <i class="fa fa-arrow-down"></i> Show More
        </button>
    </div>
    @endif
@else
<div class="feed-activity-list"></div>
<div class="jumbotron">
    <h2>No notes for this account</h2>
</div>
@endif
<script type="text/javascript">

    // ajaxed notes create
    $('#notes_form').submit(function() {
        $.post($(this).attr('action'), $(this).serialize(), function(r) {
            $('.jumbotron').hide();
            // refresh notes
            $('.feed-activity-list').load('/admin/notes?page=1&notable_type=User&notable_id='+{!! $user->id !!});
            // clear notes form
            $('#notes_text').val('');
        });
    });

    var page = 1;
    $('#more_notes').click(function() {
        page++;
        $.get('/admin/notes?page='+page+'notable_type=User&notable_id='+{!! $user->id !!}, function(response) {
            if (response == "")
            {
                $('#more_notes').hide();
            }
            else
            {
                $('.feed-activity-list').append(response);
            }
        });
    }); 
</script>