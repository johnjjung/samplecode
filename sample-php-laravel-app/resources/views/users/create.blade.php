@extends('layouts.default')

@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <h2>Invite User to this Account</h2>
                    @include('partials.error_bar')
                    {!! Form::model(@$users, ['method' => 'POST', 'route' => 'users.store', 'class' => 'form-horizontal']) !!}
                    @include('users.form')
                    
                    {{-- also send invite email --}}
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <div class="checkbox i-checks">
                                <label>
                                    <input type="checkbox" name="send_invite" value="true" checked=""> <i></i> Send Invite Email
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
