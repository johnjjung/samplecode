<div class="col-md-3">
    <div class="ibox float-e-margins">
        <div>
            <div class="ibox-content no-padding border-left-right">
                {!! HTML::image(@$user->profile_pic, null, ['class' => 'img-responsive']) !!}
            </div>
            <div class="ibox-content profile-content">
                <h2>
                <strong>{!! @$user->name !!}</strong>
                </h2>
                @if (!empty($user->account_id))
                <h5>
                <a href="{!! route('accounts.show', @$user->account_id) !!}" class="btn btn-xs btn-link">
                    {!! HTML::image(@$user->account->profile_pic, null, ['class' => 'img-circle img-responsive', 'width' => '100%']) !!}
                    {!! @$user->account->name !!}
                </a>
                </h5>
                @endif
                @if ($user->description)
                <h5>
                About
                </h5>
                <p>
                    {!! @$user->description !!}
                </p>
                @endif
                <div class="user-button">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="#message" class="btn btn-primary btn-sm btn-block" id="private_message" onclick="$('#private-message-modal').modal();"><i class="fa fa-envelope"></i> Send Private Message</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>