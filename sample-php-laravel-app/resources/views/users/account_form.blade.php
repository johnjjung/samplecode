@include('partials.error_bar')
{!! Form::open(['method' => 'PATCH', 'class' => 'form-horizontal', 'url' => "/admin/users/{$user->id}"]) !!}
{!! Form::select('account_id', $accounts, $user->account_id, ['class' => 'form-control m-b', 'required' => 'required', 'id' => 'select_account', 'placeholder' => 'Select Account']) !!}
<button class="btn btn-primary" type="submit">Assign User to this Account</button>
{!! Form::close() !!}