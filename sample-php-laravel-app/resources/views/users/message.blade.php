<div id="private-message-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                <h4 class="modal-title">
                    Send Private Message to {!! $user->name !!}
                </h4>
            </div>

            <div class="modal-body">
                {!! Form::model(@$message, ['method' => 'POST', 'route' => 'messages.store', 'class' => 'form-horizontal']) !!}
                <br>
                <div class="form-group">
                    <div class="col-sm-12">
                        {!! Form::textarea('message', null, ['class' => 'form-control', 'placeholder' => 'Type your message here..', 'rows' => 3]) !!}
                        {!! Form::hidden('subject', $user->name) !!}
                        {!! Form::hidden('recipients[]', $user->id) !!}
                        <br/>
                        
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                {!! Form::submit("Send Message", ['class' => 'btn btn-success']) !!}  
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>