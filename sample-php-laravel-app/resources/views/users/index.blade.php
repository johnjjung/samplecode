@extends('layouts.default')

@section('heading')
<h1>Users</h1>
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInUp">

            <div class="row">
                @if (!$users->isEmpty())

                @foreach(@$users as $user)
                <div class="col-lg-4">
                    <div class="contact-box">
                        <a href="{!! route('users.show', $user->id) !!}">
                            <div class="col-sm-4">
                                <div class="text-center">
                                   {{--  <img alt="image" class="img-circle m-t-xs img-responsive" src=""> --}}
                                   {{-- TODO initials --}}
                                   {!! HTML::image(@$user->profile_pic, null, ['class' => 'img-circle m-t-xs img-responsive']) !!}
                                    {{-- <div class="m-t-xs font-bold">Graphics designer</div> --}}
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <h3><strong>{!! $user->name !!}</strong></h3>
                                @if (@$user->account_id)
                                <p>
                                    {!! HTML::image($user->account->profile_pic, null, ['class' => 'img-circle img-responsive', 'title data-original-title' => $user->account->name, 'data-placement' => 'top', 'data-toggle' => 'tooltip']) !!}
                                    <i class="fa fa-building"></i>
                                    <a href="{!! route('accounts.show', $user->account_id) !!}" class="btn btn-xs btn-link"> {!! @$user->account->name !!} </a>
                                </p>
                                @else
                                <p>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                </p>
                                @endif
                                <address>
                                    {{-- space address --}}
                                    <abbr title="Email">E:</abbr> {!! $user->email !!}
                                </address>
                                <div class="pull-right">
                                    <a href="{!! route('users.edit', $user->id) !!}" class="btn btn-xs btn-link"> edit </a> </div>
                                </div>
                            <div class="clearfix"></div>
                        </a>
                    </div>

                </div>
                @endforeach
                @else

                @endif
                
            </div>
        </div>
    </div>
</div>
@stop

@section('tail')

<!-- Steps -->
{!! HTML::script('js/plugins/staps/jquery.steps.min.js'); !!}

<!-- Jquery Validate -->
{!! HTML::script('js/plugins/validate/jquery.validate.min.js'); !!}

<script type="text/javascript">
    $('.img-circle').tooltip();
</script>

@endsection
