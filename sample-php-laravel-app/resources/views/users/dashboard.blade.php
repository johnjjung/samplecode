@extends('layouts.default')

@section('heading')
<h1>Dashboard</h1>
@endsection

@section('content')


<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInUp">

            @if(empty(Auth::user()->portal_id))
            <div class="text-center">
                        
                        <h2>
                            I am a..
                        </h2>
                        <br>
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="/admin/portals/create">
                                    <img src="/img/stock/owner.png" width="100%">
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <a href="/accounts/renter">
                                    <img src="/img/stock/tenant.png" width="100%">
                                </a>
                            </div>
                        </div>
            @else
            {{-- @if(false) --}}
                {{-- dashboard icons --}}
                @foreach(@$dashboards as $dashboard)
                    @if($dashboard->is_dashboard)
                    <div class="col-sm-6 col-md-3">
                        @include('dashboards.icon')
                    </div>
                    @endif

                @endforeach
            @endif
        </div>
    </div>
</div>
@endsection

@section('tail')
@endsection
