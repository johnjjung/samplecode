<br>
{{-- account --}}
<div class="form-group">
    {!! Form::label('name', 'User Full Name', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => true]) !!}
        <small class="text-danger">{{ $errors->first('name') }}</small>
    </div>
</div>

<div class="form-group">
    {!! Form::label('email', 'User Email', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('email', null, ['class' => 'form-control', 'required' => true]) !!}
        <small class="text-danger">{{ $errors->first('email') }}</small>
    </div>
</div>

<div class="form-group">
    {!! Form::label('role', 'Member Type', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::select('role', $roles, null, ['class' => 'form-control selectize', 'id' => 'role']) !!}
        <small class="text-danger">{{ $errors->first('role') }}</small>
        <small>Admin's have more access and privileges than members.</small>
    </div>
</div>

<div class="form-group">
    {!! Form::label('description', 'About', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        <small class="text-danger">{{ $errors->first('description') }}</small>
    </div>
</div>

<div class="hr-line-dashed"></div>
{!! Form::hidden('is_active', true) !!}
{!! Form::hidden('account_id', Auth::user()->account_id) !!}
{!! Form::hidden('portal_id', Auth::user()->portal_id) !!}
