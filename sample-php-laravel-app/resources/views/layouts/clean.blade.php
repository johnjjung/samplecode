<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes" />

    <title>Dashspace</title>

    {!! HTML::style('/assets/lib.css') !!}
    {!! HTML::style('/css/style.css') !!}

    @include('partials.google_analytics')
    
    <!-- Mainly scripts -->
    {!! HTML::script('/assets/lib.js') !!}
    
    @yield('head')

</head>

<body class="skin-1 top-navigation boxed-layout">

    <div id="wrapper">
        <div id="page-wrapper">
            @include('partials.heading')
            {{-- @include('flash::message') --}}
            @yield('content')

        </div>
    </div>

    <!-- Custom and plugin javascript -->
    @include('partials.flash')
    @include('partials.compose')
    @include('partials.javascript')

    @yield('tail')

</body>

</html>
