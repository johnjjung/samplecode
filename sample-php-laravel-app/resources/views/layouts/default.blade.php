<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes" />

    <title>Dashspace</title>

    {!! HTML::style('/assets/lib.css') !!}
    {!! HTML::style('/css/style.css') !!}

    @include('partials.google_analytics')
    
    <!-- Mainly scripts -->
    {!! HTML::script('/assets/lib.js') !!}
    
    {{-- vue --}}
    @if(App::environment('local'))
    <script type="text/javascript" src="/assets/vue/dist/vue.js"></script>
    @else
    <script type="text/javascript" src="http://cdn.jsdelivr.net/vue/1.0.17/vue.min.js"></script>
    @endif

    @yield('head')

</head>

<body class="skin-1 fixed-navbar">

    <div id="wrapper">

        @if(Entrust::hasRole(['root', 'portal_owner', 'portal_admin', 'portal_employee']))
            @include('partials.admin_sidebar')
        @else
            @include('partials.sidebar')
        @endif
        <div id="page-wrapper">
            <div class="row border-bottom">
                @include('partials.navigation')
            </div>
            @include('partials.heading')
            {{-- @include('flash::message') --}}
            @yield('content')

        </div>
    </div>

    <!-- Custom and plugin javascript -->
    @include('partials.flash')
    @include('partials.compose')
    @include('partials.javascript')

    @yield('tail')

</body>

</html>
