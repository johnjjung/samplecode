@extends('layouts.preauth')

@section('content')

<h3>Open a Dashspace Account</h3>
{!! BootForm::open(['class' => 'm-t'])->action('/auth/register') !!}
    {!! BootForm::text('Full Name', 'name')->hideLabel()->placeholder("Full Name") !!}
    {!! BootForm::email('Email', 'email')->hideLabel()->placeholder("Email") !!}
    {!! BootForm::password('Password', 'password')->hideLabel()->placeholder("Password") !!}
    {!! BootForm::password('Password', 'password_confirmation')->hideLabel()->placeholder("Confirm Password") !!}
    {!! BootForm::token() !!}
    <div class="form-group">
        <div class="checkbox i-checks"><label> <input id="terms" name="terms" type="checkbox"><i></i>  By clicking this check box, I agree to the terms and privacy policies</label></div>
    </div>
    <button type="submit" class="btn btn-primary block full-width m-b">Register</button>

    <p class="text-muted text-center"><small>Already have an account?</small></p>
    <a class="btn btn-sm btn-white btn-block" href="{{ url('/auth/login') }}">Login</a>
{!! BootForm::close() !!}

{!! HTML::script('/js/jquery-2.1.1.js'); !!}


@endsection