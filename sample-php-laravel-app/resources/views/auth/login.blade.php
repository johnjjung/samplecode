@extends('layouts.preauth')

@section('content')
<h3>Login</h3>
{!! BootForm::open(['class' => 'm-t'])->action('/auth/login') !!}
    {!! BootForm::email('Email', 'email')->hideLabel()->placeholder("Email") !!}
    {!! BootForm::password('Password', 'password')->hideLabel()->placeholder("Password") !!}
    {!! BootForm::checkbox('Remember Me', 'remember') !!}
    {!! BootForm::token() !!}
    {!! BootForm::submit('Login')->class('btn btn-primary block full-width m-b') !!}

    <a href="{!! url('/password/email') !!}"><small>Forgot password?</small></a>
    @if(empty(Session::get('portal', null)))
    <p class="text-muted text-center"><small>Do not have an account?</small></p>
    <a class="btn btn-sm btn-white btn-block" href="{!! url('/auth/register') !!}">Create an account</a>
    @endif
{!! BootForm::close() !!}

@endsection