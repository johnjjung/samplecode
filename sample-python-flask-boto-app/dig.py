import utilities
import argparse
import boto
import os



def load_worker():
	bucket_name = 'jobworker-jars'
	# LOCAL_PATH = "/Users/johnjung/"
	LOCAL_PATH = "/home/ec2-user/"
	jobworker_name = 'jobworker.jar'
	# connect to the bucket
	bucket = conn.get_bucket(bucket_name)
	# go through the list of files
	bucket_list = bucket.list()
	for l in bucket_list:
		keyString = str(l.key)
		if keyString == jobworker_name:
			if not os.path.exists(LOCAL_PATH+jobworker_name):
				l.get_contents_to_filename(LOCAL_PATH+jobworker_name)
	return True

def test_mail(message):
	m = utilities.MandrillEmail()
	try:
		message = message + str(" LOGS WOULD GO HERE")
		r = m.build_request("test", "JOB WORKER", message)
		m.send_mail(r)
		return True
	except:
		return False

# get arguments from command line
def arguments():
	parser = argparse.ArgumentParser()
	parser.add_argument('nums', nargs=1)
	args = parser.parse_args()

	return args.nums[0]

def main():
	load_worker()
	test_mail(arguments())

if __name__ == "__main__":
	main()