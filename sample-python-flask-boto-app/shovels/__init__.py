__author__ = 'johnjung'

import boto.ec2
import time
import fabric
from fabric.api import *
from fabric.tasks import execute
from pprint import pprint
import xml.etree.ElementTree as ET


"""
Bot Class creates, destroys, and reads EC2 servers
"""
class Bot(object):
	def __init__(self):
		# TODO config file
		self.aws_aki = ''
		self.aws_sak = ''
		self.region = 'us-east-1'
		self.conn = boto.ec2.connect_to_region(self.region,
			aws_access_key_id=self.aws_aki,
			aws_secret_access_key=self.aws_sak)
		self.ssh_user = 'ec2-user'


	# get list of instances
	def duck_call(self):
		reservations = self.conn.get_all_instances()
		instances = [i for r in reservations for i in r.instances]
		for instance in instances:
			print instance
			# pprint(instance.__dict__)
		return instances

	# run full lifecycle
	def lifecycle(self, param):
		# creates and ec2
		instance = self.spawn(param)
		# executes commands on ec2
		self.hatch(instance, param)
		# destroy ec2
		return self.destroy(instance)

	def script_run(self, param):
		# run("uname -a")
		try:
			run("uname -a")
		except:
			print "CHECK - check failed"
			pass
		# run dig
		try:
			run("python27 /home/ec2-user/pailandshovel/dig.py " + str(param), shell=False)
		except:
			print "PYTHON SCRIPT - dig script failed"
			pass
		try:
			run("ls -a", shell=False)
		except:
			print "PYTHON SCRIPT - dig script failed"
			pass
		# run java script
		# try:
		# 	# run("java -jar /home/ec2-user/jobworker.jar", shell=False)
		# except:
		# 	print "JAVA SCRIPT - java script failed"
		# 	pass
		# return True

	# use fabric to ssh in
	def hatch(self, instance_object, param):
		print "Hatching..."
		# build host string
		env.user = self.ssh_user
		env.host_string = instance_object.public_dns_name
		env.abort_on_prompts = True
		# TODO config file
		# env.key_filename = ['/Users/johnjung/.ssh/test.pem']
		env.key_filename = ['/home/wsgi/.ssh/test.pem']
		env.parallel = True
		env.pool_size = 2
		execute(self.script_run, param, hosts=env.host_string)
		print "Matured!"

	# spawn an ec2
	def spawn(self, job_id):
		print "EC2 Spawning..."
		# TODO config file
		reservation = self.conn.run_instances(
			image_id='',  # ami image
			key_name='', # pem file
			instance_type='t2.micro',  # instance type
			security_group_ids=['']) # attach security group
		instance = reservation.instances[0]
		# tag the instances
		self.conn.create_tags([instance.id], {"Name": job_id})
		self.conn.create_tags([instance.id], {"Type": "Shovel"})
		while instance.state != 'running':
			time.sleep(5)
			instance.update()
		print "EC2 Born!"
		# added 90 seconds for initialization scripts
		time.sleep(90)
		return instance

	# terminate an ec2
	def destroy(self, instance_object):
		print "EC2 Terminating..."
		reservation = self.conn.terminate_instances(instance_object.id)
		while instance_object.state != 'terminated':
			time.sleep(5)
			instance_object.update()
		print "EC2 Buried!"
		return instance_object

	# tell salesforce response received
	def salesforce_response(self):
		return '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><notifications xmlns="http://soap.sforce.com/2005/09/outbound"><Ack>true</Ack></notifications></soapenv:Body></soapenv:Envelope>'