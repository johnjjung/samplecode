__author__ = 'johnjung'


import requests
import json


"""
PocketKnife for utilities
"""
class PocketKnife(object):
    def __init__(self):
        pass


"""
MandrillEmail Class will send emails
"""
class MandrillEmail(object):
    def __init__(self):
        # TODO config file
    # build request array
    def build_request(self, to, subject, message):
        mandrill_array = {
            "key": self.key,
            "message": {
                "html": message,
                "text": message,
                "subject": subject,
                "from_email": self.from_email,
                "from_name": self.name,
                "to": [
                    {
                        "email": to,
                        "name": self.name,
                        "type": "to"
                    }
                ],
                "headers": {
                    "Reply-To": self.from_email
                }
            }
        }
        return mandrill_array
    # sends email 
    def send_mail(self, data):

        headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
        r = requests.post(self.url, data=json.dumps(data), headers=headers)

        return r


