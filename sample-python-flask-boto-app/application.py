# -*- coding: utf-8 -*-
"""
Wherever smart people work, doors are unlocked. -- Steve Wozniak
"""

__author__ = 'johnjjung@me.com'

from flask import Flask, request
from flask import request
from flask import Response
from flask.ext import restful
from pprint import pprint
import json
import threading
import shovels
import logging
import BeautifulSoup

from logging.handlers import RotatingFileHandler


"""
Flask Server API
"""
application = Flask(__name__)
api = restful.Api(application)

#Set application.debug=true to enable tracebacks on Beanstalk log output.
#Make sure to remove this line before deploying to production.
application.debug=True

# receive params from a post request
class Blueprint(restful.Resource):

    # Get request to get statuses of all instances
    def get(self):
        digs = shovels.Bot()
        dig = digs.duck_call()

        for i, d in enumerate(dig):
            # return instance info
            dig[i] = str(d.__dict__)
        return dig
    # Post request to start a dig
    def post(self):

        xml = BeautifulSoup.BeautifulSoup(request.data)
        print "Job Id: "+str(xml.id.text)
        token = str(xml.findAll('sf:job_manager_authentication_key__c')[0].text).strip()
        auth_key = ""
        #match token
        if token == auth_key:
            try:
                # start the dig
                dig = shovels.Bot()
                threading.Thread(target=dig.lifecycle, args=(xml.id.text,)).start()
                return Response(dig.salesforce_response(), mimetype='text/xml')
            except:
                return False

api.add_resource(Blueprint, '/blueprint')

@application.route('/')
def webroot():
    return "Your Pail is in Full Swing!"

if __name__ == '__main__':
    application.run(host='0.0.0.0')