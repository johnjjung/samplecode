# Sample Code #

by John Jung (johnjjung@me.com)

Note: these apps will not necessarily run on your machine given that apikeys, passwords, entire files etc.. have been sanitized. Sample code is purely here to demonstrate basic skillsets and practices:

* SOLID Principles/Object Oriented Design
* Git/GitFlow
* AWS elasticbeanstalk (check ebextensions)
* Environment Variables (.env to hide apikeys/passwords from app)
* Dependency Injection (check laravel app)
* Eventing/Queuing Services via AWS SQS (check laravel app)
* Use of venv (check python app)
* Use of requirements.txt (check python app)
* Use of NoSQL - MongoDB (check ionic app)
* Use of SQL - MySQL (check laravel app)
* Use of acceptance/functional/unit testing Selenium/Codeception (check laravel app)
* Use of CRON jobs (check scheduler with artisan commands)
* Use of grunt/gulp/elixir/pip/composer/npm/bower
* API integrations twilio, mandrill, boto
* Use of Eloquent ORM (check laravel app)